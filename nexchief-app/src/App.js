import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import ProductPage from "./pages/Product";
import Sales from "./pages/Sales";
import SalesInvoiceItem from "./pages/SalesInvoiceItem";
import SalesInvoiceNew from "./pages/SalesInvoiceNew";
import Register from "./pages/Register";
import DashboardPage from "./pages/DashboardPage";
import Page404 from "./pages/Page404";
import ChangePass from "./pages/ChangePass";
import ReportPage from "./pages/ReportPage";

function App() {
    return (
        <Router>
            <Switch>
                <Route
                    exact
                    path='/'
                    component={
                        localStorage.getItem("username") === null ||
                        localStorage.getItem("fullName") === null
                            ? Login
                            : Home
                    }
                />
                <Route exact path='/register' component={Register} />
                <Route exact path='/product' component={ProductPage} />
                <Route exact path='/sales' component={Sales} />
                <Route exact path='/report' component={ReportPage} />
                <Route exact path='/dashboard' component={DashboardPage} />
                <Route exact path='/change-pass' component={ChangePass} />
                <Route
                    exact
                    path='/sales/:username/detail/:id'
                    component={SalesInvoiceItem}
                />
                <Route exact path='/sales/new' component={SalesInvoiceNew} />
                <Route component={Page404} />
            </Switch>
        </Router>
    );
}

export default App;
