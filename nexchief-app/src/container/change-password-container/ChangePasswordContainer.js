import React, { Component } from "react";
import ChangePassword from "../../components/change-password/ChangePassword";
import Navbar from "../../components/main-navbar/Navbar";
import APIServices from "../../services/APIServices";
import "./ChangePasswordContainer.css";
import Swal from "sweetalert2";
import { withRouter } from "react-router";

class ChangePasswordContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            currentPassword: "",
            newPassword: "",
            retypeNewPassword: "",
            error: {
                newPasswordError: "",
                retypeNewPasswordError: "",
                currentPasswordError: "",
            },
        };
    }

    componentDidMount() {
        APIServices.ReadDataByParam("users", localStorage.getItem("username"))
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    if (
                        data.payload.fullName ===
                        localStorage.getItem("fullName")
                    ) {
                        this.setState({
                            userData: data.payload,
                        });
                    } else {
                        localStorage.clear();
                        window.location.reload();
                    }
                }
            });
    }

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });
        this.setState({ error: {} });
    };

    dataToSend = () => {
        let data = {
            id: this.state.userData.id,
            email: this.state.userData.email,
            phone: this.state.userData.phone,
            fullName: this.state.userData.fullName,
            username: this.state.userData.username,
            password: this.state.newPassword,
            retypePassword: this.state.retypeNewPassword,
        };
        return data;
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const isValid = this.isValidate();
        if (isValid) {
            APIServices.createData("users/login", {
                username: this.state.userData.username,
                password: this.state.currentPassword,
            })
                .then((response) => response.json())
                .then((response) => {
                    if (response.status === true) {
                        APIServices.updateData(
                            "users/password",
                            this.dataToSend()
                        )
                            .then((response) => response.json())
                            .then((response) => {
                                if (response.status === true) {
                                    Swal.fire({
                                        position: "top-end",
                                        icon: "success",
                                        title: "Password change successful!",
                                        showConfirmButton: false,
                                        timer: 1500,
                                    }).then(() => this.props.history.push("/"));
                                } else {
                                    console.error(response.messages);
                                    Swal.fire(
                                        "Error",
                                        "Sorry, there was a problem with your request.",
                                        "error"
                                    );
                                }
                            })
                            .catch((error) => {
                                console.error(error);
                                Swal.fire(
                                    "Error",
                                    "Sorry, there was a problem with your request.",
                                    "error"
                                );
                            });
                    } else {
                        this.setState({
                            error: { currentPasswordError: "Wrong password!" },
                        });
                    }
                })
                .catch((error) => {
                    console.error(error);
                    Swal.fire(
                        "Error",
                        "Sorry, there was a problem with your request.",
                        "error"
                    );
                });
        }
    };

    isValidate = () => {
        let newPasswordError = "";
        let retypeNewPasswordError = "";

        if (this.state.newPassword === "") {
            newPasswordError = "Password is required";
        } else if (
            !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(
                this.state.newPassword
            )
        ) {
            newPasswordError =
                "Password must contain minimum 8 characters, one or more uppercase letter, one or more lowercase letter and one or more number";
        }

        if (this.state.newPassword !== this.state.retypeNewPassword) {
            retypeNewPasswordError =
                "Retype password didn't match with password";
        }

        if (
            this.state.userData.username !== localStorage.getItem("username") ||
            this.state.userData.fullName !== localStorage.getItem("fullName")
        ) {
            localStorage.clear();
            window.location.reload();
        }

        if (newPasswordError || retypeNewPasswordError) {
            this.setState({
                error: {
                    newPasswordError,
                    retypeNewPasswordError,
                },
            });
            return false;
        }
        return true;
    };

    render() {
        return (
            <div className='change-pass'>
                <Navbar />
                <div className='change-pass-card'>
                    <ChangePassword
                        {...this.state}
                        handleChange={this.handleChange}
                        handleSubmit={this.handleSubmit}
                    />
                </div>
            </div>
        );
    }
}

export default withRouter(ChangePasswordContainer);
