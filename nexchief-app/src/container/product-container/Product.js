import React, { Component } from "react";
import FormProduct from "../../components/form-product/FormProduct";
import TableProduct from "../../components/table-product/TableProduct";
import "./Product.css";
import getLocalDateTime from "../../utils/getLocalDateTime";
import getLocalDate from "../../utils/getLocalDate";
import ButtonNavbar from "../../components/button-navbar/ButtonNavbar";
import APIServices from "../../services/APIServices";
import Swal from "sweetalert2";

export default class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            code: "",
            name: "",
            packaging: "",
            description: "",
            category: "",
            launchDate: getLocalDate(),
            status: "",
            buyingPrice: "",
            sellingPrice: "",
            createdBy: localStorage.getItem("username"),
            createdAt: getLocalDateTime(),
            updatedBy: localStorage.getItem("username"),
            updatedAt: getLocalDateTime(),
            error: {},
            productEdit: {},
            isDisabled: false,
            isEditable: false,
            productList: [],
            userData: {},
        };
    }

    componentDidMount() {
        APIServices.ReadData("products/all")
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    Swal.fire("Error", "Failed to choose product.", "error");
                } else {
                    this.setState({
                        ...this.state,
                        productList: data.payload,
                    });
                }
            });
        APIServices.ReadDataByParam("users", localStorage.getItem("username"))
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    if (
                        data.payload.fullName ===
                        localStorage.getItem("fullName")
                    ) {
                        this.setState({
                            userData: data.payload,
                        });
                    } else {
                        localStorage.clear();
                        window.location.reload();
                    }
                }
            });
    }

    isValidate = () => {
        let nameError = "";
        let packagingError = "";
        let categoryError = "";
        let codeError = "";
        let codeList = this.state.productList.map((a) => a.code);

        if (!this.state.name) {
            nameError = "Product name is required";
        }
        if (!this.state.packaging) {
            packagingError = "Packaging is required";
        }
        if (!this.state.category) {
            categoryError = "Product category is required";
        }
        if (
            codeList.includes(this.state.code.toUpperCase()) &&
            !this.state.id
        ) {
            codeError = "Product code already in use";
        } else if (
            !/^[A-Z]{2,3}-[A-Z]{2,3}-[0-9]*$/.test(
                this.state.code.toUpperCase()
            ) &&
            this.state.code
        ) {
            codeError = "Invalid product code";
        }
        if (nameError || packagingError || categoryError || codeError) {
            this.setState({
                error: {
                    nameError,
                    packagingError,
                    categoryError,
                    codeError,
                },
            });
            return false;
        }
        return true;
    };

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });
        this.setState({ error: {} });
    };

    handleSubmit = (event) => {
        console.log(this.state.code);
        event.preventDefault();
        const isValid = this.isValidate();
        if (isValid) {
            if (this.state.id === "") {
                this.postData();
            } else {
                this.updateData();
            }
        }
    };

    dataToSubmit = (tipe) => {
        let data = {
            id: this.state.id,
            code: this.state.code.toUpperCase(),
            name: this.state.name.toUpperCase(),
            packaging: this.state.packaging.toUpperCase(),
            description: this.state.description.toUpperCase(),
            category: this.state.category.toUpperCase(),
            launchDate: this.state.launchDate,
            status: this.state.status.toUpperCase(),
            buyingPrice:
                this.state.buyingPrice === ""
                    ? 0
                    : this.state.buyingPrice.toString().replace(".", ""),
            sellingPrice:
                this.state.sellingPrice === ""
                    ? 0
                    : this.state.sellingPrice.toString().replace(".", ""),
            createdBy:
                tipe === "create"
                    ? localStorage.getItem("username")
                    : this.state.productEdit.createdBy,
            updatedBy:
                tipe === "create"
                    ? this.state.updatedBy
                    : localStorage.getItem("username"),
        };
        return data;
    };

    updateData = () => {
        APIServices.updateData("products", this.dataToSubmit("update"))
            .then((response) => response.json())
            .then((response) => {
                if (response.status === true) {
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "Success update product!",
                        showConfirmButton: false,
                        timer: 1500,
                    }).then(() => window.location.reload());
                } else {
                    console.error(response.messages);
                    Swal.fire(
                        "Error",
                        "Sorry, there was a problem with your request.",
                        "error"
                    );
                }
            })
            .catch((error) => {
                console.error(error);
                Swal.fire(
                    "Error",
                    "Sorry, there was a problem with your request.",
                    "error"
                );
            });
    };

    postData = () => {
        APIServices.createData("products", this.dataToSubmit("create"))
            .then((response) => response.json())
            .then((response) => {
                if (response.status === true) {
                    Swal.fire({
                        position: "top-end",
                        icon: "success",
                        title: "Success add product!",
                        showConfirmButton: false,
                        timer: 1500,
                    }).then(() => window.location.reload());
                } else {
                    console.error(response.messages);
                    Swal.fire(
                        "Error",
                        "Sorry, there was a problem with your request.",
                        "error"
                    );
                }
            })
            .catch((error) => {
                console.error(error);
                Swal.fire(
                    "Error",
                    "Sorry, there was a problem with your request.",
                    "error"
                );
            });
    };

    getDataById = (id) => {
        const selectedItems = this.state.productList.find(
            (values) => String(values.id) === String(id)
        );
        if (selectedItems) {
            this.setState({
                id: selectedItems.id,
                code: selectedItems.code,
                name: selectedItems.name,
                packaging: selectedItems.packaging,
                description: selectedItems.description,
                category: selectedItems.category,
                launchDate: selectedItems.launchDate,
                status: selectedItems.status,
                buyingPrice: selectedItems.buyingPrice,
                sellingPrice: selectedItems.sellingPrice,
                createdAt: selectedItems.createdAt,
                createdBy: selectedItems.createdBy,
                updatedAt: selectedItems.updatedAt,
                updatedBy: selectedItems.updatedBy,
                isDisabled: true,
                isEditable: true,
                error: "",
                productEdit: selectedItems,
            });
        }
    };

    deleteData = () => {
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes",
        }).then((result) => {
            if (result.isConfirmed) {
                APIServices.deleteData("products", this.state.id)
                    .then((res) => res.status)
                    .catch((error) => {
                        console.error(error);
                        Swal.fire(
                            "Error",
                            "Sorry, there was a problem when delete product.",
                            "error"
                        );
                    });
            }
            Swal.fire(
                "Deleted!",
                "The product has been deleted.",
                "success"
            ).then(() => window.location.reload());
        });
    };

    handleEdit = (event) => {
        event.preventDefault();
        this.setState({ isDisabled: false, isEditable: true });
    };

    cancelEdit = () => {
        let data = this.state.productEdit;
        this.setState({
            id: data.id,
            code: data.code,
            name: data.name,
            packaging: data.packaging,
            description: data.description,
            category: data.category,
            launchDate: data.launchDate,
            status: data.status,
            buyingPrice: data.buyingPrice,
            sellingPrice: data.sellingPrice,
            createdAt: data.createdAt,
            createdBy: data.createdBy,
            updatedAt: data.updatedAt,
            updatedBy: data.updatedBy,
            isDisabled: true,
            error: {},
        });
    };

    refreshStateToEmpty = () => {
        this.setState({
            id: "",
            code: "",
            name: "",
            packaging: "",
            description: "",
            category: "",
            launchDate: getLocalDate(),
            status: "",
            buyingPrice: "",
            sellingPrice: "",
            createdAt: getLocalDateTime(),
            createdBy: localStorage.getItem("username"),
            updatedAt: getLocalDateTime(),
            updatedBy: localStorage.getItem("username"),
            error: {},
            productEdit: {},
            isDisabled: false,
            isEditable: false,
        });
    };

    render() {
        return (
            <>
                <ButtonNavbar
                    {...this.state}
                    navbarUse='product'
                    delete={this.deleteData}
                    submitHandler={this.handleSubmit}
                    editHandler={this.handleEdit}
                    cancelEdit={this.cancelEdit}
                    refreshForm={this.refreshStateToEmpty}
                />
                <div className='product'>
                    <TableProduct getId={this.getDataById} />
                    <FormProduct
                        {...this.state}
                        handleChange={this.handleChange}
                    />
                </div>
            </>
        );
    }
}
