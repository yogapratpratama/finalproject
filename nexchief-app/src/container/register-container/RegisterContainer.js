import React, { Component } from "react";
import RegisterForm from "../../components/login-register/RegisterForm";
import "./RegisterContainer.css";
import APIServices from "../../services/APIServices";
import Swal from "sweetalert2";
import { withRouter } from "react-router-dom";

class RegisterContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataToSubmit: {
                username: "",
                fullName: "",
                email: "",
                phone: "",
                password: "",
                retypePassword: "",
            },
            error: {},
            userData: [],
        };
    }

    componentDidMount() {
        APIServices.ReadData("users")
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    console.error(data.messages.map((errorMsg) => errorMsg));
                } else {
                    this.setState({
                        userData: data.payload,
                    });
                }
            });
    }

    isValidate = () => {
        const data = this.state.dataToSubmit;
        let usernameError = "";
        let fullNameError = "";
        let emailError = "";
        let phoneError = "";
        let passwordError = "";
        let retypePasswordError = "";
        let username = this.state.userData.findIndex(
            (i) => i.username === data.username
        );
        let email = this.state.userData.findIndex(
            (i) => i.email === data.email
        );
        let phone = this.state.userData.findIndex(
            (i) => i.phone === data.phone
        );

        // username validation
        if (data.username === "") {
            usernameError = "Username is required";
        } else if (!/^[\w](?!.*?\.{2})[\w.]{1,18}[\w]$/.test(data.username)) {
            usernameError = "Your username is incorrect";
        } else if (username !== -1) {
            usernameError = "This username has already been taken";
        }

        // full name validation
        if (data.fullName === "") {
            fullNameError = "Full Name is required";
        }

        // email validation
        if (data.email === "") {
            emailError = "Email is required";
        } else if (!/\S+@\S+\.\S+/.test(data.email)) {
            emailError = "Invalid email address";
        } else if (email !== -1) {
            emailError = "This email has already been taken";
        }

        // phone number validation
        if (data.phone === "") {
            phoneError = "Phone number is required";
        } else if (!/^(0)8[1-9][0-9]{8,10}$/.test(data.phone)) {
            phoneError = "Invalid phone number";
        } else if (phone !== -1) {
            phoneError = "This phone number has already been taken";
        }

        // password validation
        if (data.password === "") {
            passwordError = "Password is required";
        } else if (
            !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(
                data.password
            )
        ) {
            passwordError =
                "Password must contain minimum 8 characters, one or more uppercase letter, one or more lowercase letter and one or more number";
        }

        if (data.password !== data.retypePassword) {
            retypePasswordError = "Retype password didn't match with password";
        }

        if (
            usernameError ||
            fullNameError ||
            emailError ||
            phoneError ||
            passwordError ||
            retypePasswordError
        ) {
            this.setState({
                error: {
                    usernameError,
                    fullNameError,
                    emailError,
                    phoneError,
                    passwordError,
                    retypePasswordError,
                },
            });
            return false;
        }
        return true;
    };

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({
            dataToSubmit: {
                ...this.state.dataToSubmit,
                [name]: value,
            },
        });
        this.setState({ error: {} });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const isValid = this.isValidate();
        if (isValid) {
            APIServices.createData("users", this.state.dataToSubmit)
                .then((response) => response.json())
                .then((response) => {
                    if (response.status === true) {
                        Swal.fire({
                            position: "top-end",
                            icon: "success",
                            title: "Success create an account.",
                            showConfirmButton: false,
                            timer: 1500,
                        }).then(() => this.props.history.push("/"));
                    } else {
                        console.error(response.messages);
                        Swal.fire(
                            "Error",
                            "Sorry, there was a problem with your request. Try Again later.",
                            "error"
                        );
                    }
                })
                .catch((error) => {
                    console.error(error);
                    Swal.fire(
                        "Error",
                        "Sorry, there was a problem with your request. Try again later.",
                        "error"
                    );
                });
        }
    };

    render() {
        return (
            <div className='register-background'>
                <div className='register-wrapper'>
                    <div className='register-form'>
                        <RegisterForm
                            {...this.state}
                            handleChange={this.handleChange}
                            handleSubmit={this.handleSubmit}
                        />
                    </div>
                    <img
                        src='./images/registration.png'
                        alt='registration'
                        width='500px'
                    ></img>
                </div>
            </div>
        );
    }
}

export default withRouter(RegisterContainer);
