import React, { Component } from "react";
import getLocalDate from "../../utils/getLocalDate";
import APIServices from "../../services/APIServices";
import ButtonNavbar from "../../components/button-navbar/ButtonNavbar";
import { connect } from "react-redux";
import FormPenjualan from "../../components/form-penjualan/FormPenjualan";
import { withRouter } from "react-router-dom";
import Swal from "sweetalert2";
import Page404Container from "../404-container/Page404Container";

class InvoiceDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeProduct: [],
            invoiceItems: [
                {
                    id: "",
                    product: {
                        id: "",
                        code: "",
                        name: "",
                        sellingPrice: "",
                    },
                    quantity: "",
                    subTotal: 0,
                },
            ],
            id: "",
            date: getLocalDate(),
            distributor: "",
            customer: "",
            status: "Unpaid",
            totalItem: 0,
            discount: "",
            tax: 0,
            totalInvoicePrice: 0,
            error: {},
            isDisabled: true,
            invoiceEdit: {},
            user: {},
            Url: true,
        };
    }

    componentDidMount() {
        let id =
            this.props.invoiceId === ""
                ? this.props.match.params.id
                : this.props.invoiceId;

        let username = this.props.match.params.username;

        APIServices.ReadDataByParam("products", "active")
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    this.setState({ errorFetch: true });
                } else {
                    this.setState({
                        ...this.state,
                        activeProduct: data.payload,
                        errorFetch: false,
                    });
                }
            });

        APIServices.ReadDataByParam("users", localStorage.getItem("username"))
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    if (
                        data.payload.fullName ===
                        localStorage.getItem("fullName")
                    ) {
                        if (username === data.payload.username) {
                            APIServices.ReadDataByParams(
                                "invoice",
                                id,
                                "user",
                                data.payload.id
                            )
                                .then((response) => response.json())
                                .then((d) => {
                                    if (d.status === false) {
                                        this.setState({ Url: false });
                                    } else {
                                        this.setState({
                                            ...this.state,
                                            id: d.payload.id,
                                            date: d.payload.date,
                                            distributor: d.payload.distributor,
                                            customer: d.payload.customer,
                                            status: d.payload.status,
                                            totalItem: d.payload.totalItem,
                                            discount: d.payload.discount,
                                            tax: d.payload.tax,
                                            totalInvoicePrice:
                                                d.payload.totalInvoicePrice,
                                            user: d.payload.user,
                                            invoiceItems:
                                                d.payload.invoiceItems,
                                            invoiceEdit: d.payload,
                                        });
                                    }
                                })
                                .catch(() => {
                                    this.setState({ Url: false });
                                });
                        } else {
                            this.setState({ Url: false });
                        }
                    } else {
                        localStorage.clear();
                        window.location.reload();
                    }
                }
            });
    }

    isValidate = () => {
        let customerError = "";
        let distributorError = "";
        let productCodeError = "";
        let quantityError = "";
        let discountError = "";
        let emptyProductCode = this.state.invoiceItems.findIndex(
            (i) => i.product.code === ""
        );
        let emptyQuantity = this.state.invoiceItems.findIndex(
            (i) => i.quantity === ""
        );
        let zeroQuantity = this.state.invoiceItems.findIndex(
            (i) => i.quantity === 0
        );

        if (!this.state.customer) {
            customerError = "Customer is required";
        }
        if (!this.state.distributor) {
            distributorError = "Distributor is required";
        }
        if (emptyProductCode !== -1) {
            productCodeError = "Product code is required";
        }
        if (emptyQuantity !== -1 || zeroQuantity !== -1) {
            quantityError = "Quantity is required";
        }
        if (this.state.discount > this.state.totalItem) {
            discountError = "Discount must be lower than total item";
            Swal.fire(
                "Error",
                "Discount must be lower than total item.",
                "error"
            );
        }

        if (
            distributorError ||
            productCodeError ||
            quantityError ||
            customerError ||
            discountError
        ) {
            this.setState({
                error: {
                    distributorError,
                    productCodeError,
                    quantityError,
                    customerError,
                    discountError,
                },
            });
            return false;
        }
        return true;
    };

    addItem = () => {
        this.setState((prevState) => ({
            invoiceItems: [
                ...prevState.invoiceItems,
                {
                    id: "",
                    product: {
                        id: "",
                        code: "",
                        name: "",
                        sellingPrice: "",
                    },
                    quantity: "",
                    subTotal: 0,
                },
            ],
        }));
    };

    removeItem = (id) => {
        const values = [...this.state.invoiceItems];
        if (this.state.invoiceItems[id].product.name !== "") {
            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes",
            }).then((result) => {
                if (result.isConfirmed) {
                    values.splice(id, 1);
                    this.setState({ invoiceItems: values });
                }
            });
        } else {
            values.splice(id, 1);
            this.setState({ invoiceItems: values });
        }
    };

    handleSelect = (event, id) => {
        const selectedItems = this.state.activeProduct.find(
            (values) => String(values.code) === String(event.target.value)
        );
        if (selectedItems) {
            this.setState({
                invoiceItems: [
                    ...this.state.invoiceItems.slice(0, id),
                    {
                        id: "",
                        product: {
                            id: selectedItems.id,
                            code: selectedItems.code,
                            name: selectedItems.name,
                            sellingPrice: selectedItems.sellingPrice,
                        },
                        quantity: "",
                        subTotal: 0,
                    },
                    ...this.state.invoiceItems.slice(id + 1),
                ],
            });
            this.setState({ error: {} });
        }
    };

    handleChangeItem = (event, id) => {
        const invoice = this.state.invoiceItems;
        let value = event.target.value;
        const priceTemp =
            invoice[id].product.sellingPrice *
            parseInt(value.replaceAll(".", ""), 10);
        this.setState({
            invoiceItems: [
                ...invoice.slice(0, id),
                {
                    ...this.state.invoiceItems[id],
                    quantity: parseInt(value.replaceAll(".", ""), 10),
                    subTotal: priceTemp,
                },
                ...invoice.slice(id + 1),
            ],
        });
        this.setState({ error: {} });
    };

    componentDidUpdate(prevProps, prevState) {
        if (
            prevState.invoiceItems !== this.state.invoiceItems ||
            prevState.discount !== this.state.discount
        ) {
            const sumTotalItem = this.state.invoiceItems
                ? this.state.invoiceItems.reduce(
                      (total, currentValue) => total + currentValue.subTotal,
                      0
                  )
                : 0;
            this.setState({ totalItem: sumTotalItem });

            const totalAfterDiscount = sumTotalItem - this.state.discount;
            const tax = totalAfterDiscount * (10 / 100);
            this.setState({ tax: tax });

            const totalPrice = totalAfterDiscount + tax;
            this.setState({ totalInvoicePrice: totalPrice });
        }
    }

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;

        if (name === "discount") {
            this.setState({
                discount: Number(value.replaceAll(".", "")),
            });
        } else {
            this.setState({ [name]: value });
        }
        this.setState({ error: {} });
    };

    dataToSubmit = () => {
        let data = {
            id: this.state.id,
            date: this.state.date,
            distributor: this.state.distributor,
            customer: this.state.customer,
            status: this.state.status,
            totalItem: this.state.totalItem,
            discount: this.state.discount,
            tax: this.state.tax,
            totalInvoicePrice: this.state.totalInvoicePrice,
            invoiceItems: this.state.invoiceItems,
            user: this.state.user,
        };
        return data;
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const isValid = this.isValidate();
        if (isValid) {
            APIServices.updateData("invoice", this.dataToSubmit())
                .then((response) => response.json())
                .then((response) => {
                    if (response.status === true) {
                        Swal.fire({
                            icon: "success",
                            title: "Success update invoice!",
                            showConfirmButton: false,
                            timer: 1500,
                        }).then(() => window.location.reload());
                    } else {
                        console.error(response.messages);
                        Swal.fire(
                            "Error",
                            "Sorry, there was a problem with your request.",
                            "error"
                        );
                    }
                })
                .catch((error) => {
                    console.error(error);
                    Swal.fire(
                        "Error",
                        "Sorry, there was a problem with your request.",
                        "error"
                    );
                });
        }
    };

    refreshStateToEmpty = () => {
        this.setState({
            invoiceItems: [
                {
                    id: "",
                    product: {
                        id: "",
                        code: "",
                        name: "",
                        sellingPrice: "",
                    },
                    quantity: "",
                    subTotal: 0,
                },
            ],
            id: "",
            date: getLocalDate(),
            distributor: "",
            customer: "",
            status: "Unpaid",
            totalItem: 0,
            discount: "",
            tax: 0,
            total: 0,
            error: {},
        });
    };

    handleEdit = (event) => {
        event.preventDefault();
        this.setState({ isDisabled: false });
    };

    cancelEdit = () => {
        let data = this.state.invoiceEdit;
        this.setState({
            ...this.state,
            invoiceItems: data.invoiceItems,
            id: data.id,
            date: data.date,
            distributor: data.distributor,
            customer: data.customer,
            status: data.status,
            totalItem: data.totalItem,
            discount: data.discount,
            tax: data.tax,
            totalInvoicePrice: data.totalInvoicePrice,
            user: data.user,
            error: {},
            isDisabled: true,
        });
    };

    render() {
        return this.state.Url === true ? (
            <>
                <ButtonNavbar
                    navbarUse='invoiceDetail'
                    isDisabled={this.state.isDisabled}
                    status={this.state.status}
                    invoiceSubmit={this.handleSubmit}
                    editHandler={this.handleEdit}
                    cancelEdit={this.cancelEdit}
                />
                <FormPenjualan
                    {...this.state}
                    addItem={this.addItem}
                    removeItem={this.removeItem}
                    handleSelect={this.handleSelect}
                    handleChangeItem={this.handleChangeItem}
                    handleChange={this.handleChange}
                    handleSubmit={this.handleSubmit}
                />
            </>
        ) : (
            <Page404Container />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        invoiceId: state.invoiceId,
    };
};

export default withRouter(connect(mapStateToProps, null)(InvoiceDetail));
