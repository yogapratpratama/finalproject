import React, { Component } from "react";
import LoginForm from "../../components/login-register/LoginForm";
import "./LoginContainer.css";
import Swal from "sweetalert2";
import APIServices from "../../services/APIServices";

export default class LoginContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            error: "",
        };
    }

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        APIServices.createData("users/login", this.state)
            .then((response) => response.json())
            .then((response) => {
                if (response.status === true) {
                    localStorage.clear();
                    localStorage.setItem("username", response.payload.username);
                    localStorage.setItem("fullName", response.payload.fullName);
                    window.location.reload();
                } else {
                    this.setState({
                        ...this.state,
                        error: response.messages.map((data) => data),
                    });
                }
            })
            .catch((error) => {
                console.error(error);
                Swal.fire(
                    "Error",
                    "Sorry, there was a problem with your request. Try again later.",
                    "error"
                );
            });
    };

    render() {
        return (
            <div className='login-background'>
                <div className='login-wrapper'>
                    <LoginForm
                        {...this.state}
                        handleChange={this.handleChange}
                        handleSubmit={this.handleSubmit}
                    />
                </div>
            </div>
        );
    }
}
