import React, { Component } from "react";
import "./Page404Container.css";

export default class Page404Container extends Component {
    render() {
        return (
            <div className='container-404'>
                <img src='/images/error404.jpg' alt='error404' />
                <h1>Sorry, this page isn't available.</h1>
                <p>
                    The page you are looking for might have been removed, had
                    its name changed or is temporarily unavailable.{" "}
                    <a href='/' className='homelink'>Go back to NexChief.</a>
                </p>
            </div>
        );
    }
}
