import React, { Component } from "react";
import FormPenjualan from "../../components/form-penjualan/FormPenjualan";
import APIServices from "../../services/APIServices";
import getLocalDate from "../../utils/getLocalDate";
import ButtonNavbar from "../../components/button-navbar/ButtonNavbar";
import Swal from "sweetalert2";

export default class InvoiceNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeProduct: [],
            invoiceItems: [
                {
                    id: "",
                    product: {
                        id: "",
                        code: "",
                        name: "",
                        sellingPrice: "",
                    },
                    quantity: "",
                    subTotal: 0,
                },
            ],
            id: "",
            date: getLocalDate(),
            distributor: "",
            customer: "",
            status: "Unpaid",
            totalItem: 0,
            discount: "",
            tax: 0,
            totalInvoicePrice: 0,
            error: {},
            errorFetch: true,
            userData: {},
        };
    }

    componentDidMount() {
        APIServices.ReadDataByParam("products", "active")
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    this.setState({ errorFetch: true });
                } else {
                    this.setState({
                        ...this.state,
                        activeProduct: data.payload,
                        errorFetch: false,
                    });
                }
            });
        APIServices.ReadDataByParam("users", localStorage.getItem("username"))
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    if (
                        data.payload.fullName ===
                        localStorage.getItem("fullName")
                    ) {
                        this.setState({
                            userData: data.payload,
                        });
                    } else {
                        localStorage.clear();
                        window.location.reload();
                    }
                }
            });
    }

    isValidate = () => {
        let customerError = "";
        let distributorError = "";
        let productCodeError = "";
        let quantityError = "";
        let discountError = "";
        let emptyProductCode = this.state.invoiceItems.findIndex(
            (i) => i.product.code === ""
        );
        let emptyQuantity = this.state.invoiceItems.findIndex(
            (i) => i.quantity === ""
        );

        if (!this.state.customer) {
            customerError = "Customer is required";
        }
        if (!this.state.distributor) {
            distributorError = "Distributor is required";
        }
        if (emptyProductCode !== -1) {
            productCodeError = "Product code is required";
        }
        if (emptyQuantity !== -1) {
            quantityError = "Quantity is required";
        }
        if (this.state.discount > this.state.totalItem) {
            discountError = "Discount must be lower than total item";
            Swal.fire(
                "Error",
                "Discount must be lower than total item.",
                "error"
            );
        }

        if (
            distributorError ||
            productCodeError ||
            quantityError ||
            customerError ||
            discountError
        ) {
            this.setState({
                error: {
                    distributorError,
                    productCodeError,
                    quantityError,
                    customerError,
                    discountError,
                },
            });
            return false;
        }
        return true;
    };

    addItem = () => {
        this.setState((prevState) => ({
            invoiceItems: [
                ...prevState.invoiceItems,
                {
                    id: "",
                    product: {
                        id: "",
                        code: "",
                        name: "",
                        sellingPrice: "",
                    },
                    quantity: "",
                    subTotal: 0,
                },
            ],
        }));
    };

    removeItem = (id) => {
        const values = [...this.state.invoiceItems];
        values.splice(id, 1);
        this.setState({ invoiceItems: values });
    };

    handleSelect = (event, id) => {
        const selectedItems = this.state.activeProduct.find(
            (values) => String(values.code) === String(event.target.value)
        );
        if (selectedItems) {
            this.setState({
                invoiceItems: [
                    ...this.state.invoiceItems.slice(0, id),
                    {
                        id: "",
                        product: {
                            id: selectedItems.id,
                            code: selectedItems.code,
                            name: selectedItems.name,
                            sellingPrice: selectedItems.sellingPrice,
                        },
                        quantity: "",
                        subTotal: 0,
                    },
                    ...this.state.invoiceItems.slice(id + 1),
                ],
            });
            this.setState({ error: {} });
        }
    };

    handleChangeItem = (event, id) => {
        const invoice = this.state.invoiceItems;
        let value = event.target.value;
        const priceTemp =
            invoice[id].product.sellingPrice *
            parseInt(value.replaceAll(".", ""), 10);
        this.setState({
            invoiceItems: [
                ...invoice.slice(0, id),
                {
                    ...this.state.invoiceItems[id],
                    quantity: parseInt(value.replaceAll(".", ""), 10),
                    subTotal: priceTemp,
                },
                ...invoice.slice(id + 1),
            ],
        });
        this.setState({ error: {} });
    };

    componentDidUpdate(prevProps, prevState) {
        if (
            prevState.invoiceItems !== this.state.invoiceItems ||
            prevState.discount !== this.state.discount
        ) {
            const sumTotalItem = this.state.invoiceItems.reduce(
                (total, currentValue) =>
                    total + parseInt(currentValue.subTotal),
                0
            );
            this.setState({ totalItem: sumTotalItem });

            const totalAfterDiscount = sumTotalItem - this.state.discount;
            const tax = totalAfterDiscount * (10 / 100);
            this.setState({ tax: tax });

            const totalPrice = totalAfterDiscount + tax;
            this.setState({ totalInvoicePrice: totalPrice });
        }
    }

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;

        if (name === "discount") {
            this.setState({ discount: Number(value.replaceAll(".", "")) });
        } else {
            this.setState({ [name]: value });
        }
        this.setState({ error: {} });
    };

    dataToSubmit = () => {
        let data = {
            id: this.state.id,
            date: this.state.date,
            distributor: this.state.distributor,
            customer: this.state.customer,
            status: this.state.status,
            totalItem: this.state.totalItem,
            discount: this.state.discount,
            tax: this.state.tax,
            totalInvoicePrice: this.state.totalInvoicePrice,
            invoiceItems: this.state.invoiceItems,
            user: this.state.userData,
        };
        return data;
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const isValid = this.isValidate();
        if (isValid) {
            APIServices.createData("invoice", this.dataToSubmit())
                .then((response) => response.json())
                .then((response) => {
                    if (response.status === true) {
                        Swal.fire({
                            icon: "success",
                            title: "Success add invoice!",
                            showConfirmButton: false,
                            timer: 1500,
                        }).then(() => this.refreshStateToEmpty());
                    } else {
                        console.error(response.messages);
                        Swal.fire(
                            "Error",
                            "Sorry, there was a problem with your request.",
                            "error"
                        );
                    }
                })
                .catch((error) => {
                    console.error(error);
                    Swal.fire(
                        "Error",
                        "Sorry, there was a problem with your request.",
                        "error"
                    );
                });
        }
    };

    refreshStateToEmpty = () => {
        this.setState({
            invoiceItems: [
                {
                    id: "",
                    product: {
                        id: "",
                        code: "",
                        name: "",
                        sellingPrice: "",
                    },
                    quantity: "",
                    subTotal: 0,
                },
            ],
            id: "",
            date: getLocalDate(),
            distributor: "",
            customer: "",
            status: "Unpaid",
            totalItem: 0,
            discount: "",
            tax: 0,
            totalInvoicePrice: 0,
            error: {},
        });
    };

    render() {
        return (
            <>
                <ButtonNavbar
                    navbarUse='invoiceNew'
                    invoiceSubmit={this.handleSubmit}
                />
                <FormPenjualan
                    {...this.state}
                    addItem={this.addItem}
                    removeItem={this.removeItem}
                    handleSelect={this.handleSelect}
                    handleChangeItem={this.handleChangeItem}
                    handleChange={this.handleChange}
                    handleSubmit={this.handleSubmit}
                />
            </>
        );
    }
}
