import React, { Component } from "react";
import "../login-register/LoginRegister.css";
import Input from "../../atomic-components/input/Input";
import { Button } from "../../atomic-components/button/Button";
import { BsShieldLockFill } from "react-icons/bs";

export default class ChangePassword extends Component {
    render() {
        return (
            <div className='form-container' style={{ width: "400px" }}>
                <form className='form'>
                    <BsShieldLockFill
                        style={{
                            display: "block",
                            margin: "0 auto",
                            fontSize: "50px",
                        }}
                    />
                    <h1 className='login'>Change your password</h1>
                    <div className='login-card'>
                        <Input
                            label='Current Password'
                            className='form-input'
                            type='password'
                            name='currentPassword'
                            placeholder='Enter your current password'
                            value={this.props.currentPassword}
                            onChange={this.props.handleChange}
                        />
                        <p className='error-message'>
                            {this.props.error.currentPasswordError}
                        </p>
                    </div>
                    <div className='login-card'>
                        <Input
                            label='New Password'
                            className='form-input'
                            type='password'
                            name='newPassword'
                            placeholder='Enter your new password'
                            value={this.props.newPassword}
                            onChange={this.props.handleChange}
                        />
                        <p className='error-message'>
                            {this.props.error.newPasswordError}
                        </p>
                    </div>
                    <div className='login-card'>
                        <Input
                            label='Retype New Password'
                            className='form-input'
                            type='password'
                            name='retypeNewPassword'
                            placeholder='Retype your new password'
                            value={this.props.retypeNewPassword}
                            onChange={this.props.handleChange}
                        />
                        <p className='error-message'>
                            {this.props.error.retypeNewPasswordError}
                        </p>
                    </div>
                    <div className='login-card'>
                        <Button
                            type='submit'
                            className='button--box primary'
                            value='Change my password'
                            style={{ width: "100%" }}
                            onClick={this.props.handleSubmit}
                        />
                    </div>
                </form>
            </div>
        );
    }
}
