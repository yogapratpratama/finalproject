import React, { Component } from "react";
import "./TableProduct.css";
import APIServices from "../../services/APIServices";

export default class TableProduct extends Component {
    state = {
        dataProduct: [],
        totalPages: "",
        search: "",
        errorFetch: true,
        page: 0,
    };

    handleChange = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });
    };

    handlePagePlus = () => {
        this.setState(
            (prevState) => ({ page: prevState.page + 1 }),
            () => this.fetchData()
        );
    };

    handlePageMin = () => {
        this.setState(
            (prevState) => ({ page: prevState.page - 1 }),
            () => this.fetchData()
        );
    };

    fetchData = () => {
        APIServices.ReadData(
            "products?page=" +
                this.state.page +
                "&size=5&search=" +
                this.state.search
        )
            .then((response) => response.json())
            .then((data) => {
                if (data.first === true && data.empty === true) {
                    this.setState({ errorFetch: true });
                } else {
                    this.setState({
                        ...this.state,
                        dataProduct: data.content,
                        totalPages: data.totalPages,
                        errorFetch: false,
                    });
                }
            });
    };

    componentDidMount() {
        this.fetchData();
    }

    handleSearch = () => {
        this.fetchData();
    };

    render() {
        return (
            <>
                <div className='dataContainer'>
                    <div className='search-container'>
                        <input
                            className='search'
                            type='text'
                            placeholder='Search product...'
                            name='search'
                            value={this.state.search}
                            onChange={this.handleChange}
                        />
                        <input
                            type='submit'
                            value='Search'
                            className='submit-search'
                            onClick={this.handleSearch}
                        />
                    </div>
                    {this.state.errorFetch ? (
                        <div className='error-fetch'>
                            There is no product to display.
                        </div>
                    ) : (
                        <ul>
                            {this.state.dataProduct.map((data, key) => (
                                <li
                                    className='dataProduct'
                                    tabIndex='1'
                                    key={key}
                                    onClick={() => this.props.getId(data.id)}
                                >
                                    <p>
                                        <span style={{ fontWeight: "550" }}>
                                            {data.code},
                                        </span>{" "}
                                        {data.name} {data.packaging}
                                    </p>
                                    <div className='keterangan-produk'>
                                        <div className='buying'>
                                            <p>Buying</p>
                                            <p>
                                                Rp
                                                {data.buyingPrice.toLocaleString(
                                                    "id-ID"
                                                )}
                                            </p>
                                        </div>
                                        <div className='selling'>
                                            <p>Selling</p>
                                            <p
                                                style={{
                                                    fontWeight: "550",
                                                    color: "yellow",
                                                }}
                                            >
                                                Rp
                                                {data.sellingPrice.toLocaleString(
                                                    "id-ID"
                                                )}
                                            </p>
                                        </div>
                                        <div className='status'>
                                            <p>Status</p>
                                            <p>{data.status}</p>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    )}
                    {this.state.totalPages === 1 ||
                    this.state.totalPages === "" ? null : (
                        <div className='pagination'>
                            <div className='pagination-wrapper'>
                                <p
                                    className={
                                        this.state.page === 0
                                            ? "arrow limit"
                                            : "arrow"
                                    }
                                    onClick={this.handlePageMin}
                                >
                                    ❮
                                </p>
                                <p>
                                    Page {this.state.page + 1} of{" "}
                                    {this.state.totalPages}
                                </p>
                                <p
                                    className={
                                        this.state.totalPages - 1 !==
                                        this.state.page
                                            ? "arrow"
                                            : "arrow limit"
                                    }
                                    onClick={this.handlePagePlus}
                                >
                                    ❯
                                </p>
                            </div>
                        </div>
                    )}
                </div>
            </>
        );
    }
}
