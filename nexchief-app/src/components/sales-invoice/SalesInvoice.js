import React, { Component } from "react";
import "./SalesInvoice.css";
import { Button } from "../../atomic-components/button/Button";
import ButtonNavbar from "../button-navbar/ButtonNavbar";
import APIServices from "../../services/APIServices";
import { connect } from "react-redux";
import { setInvoiceId } from "../../redux/Actions";

class SalesInvoice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            invoice: [],
            userData: {},
            isEmpty: false,
            page: 0,
            totalPages: "",
        };
    }

    fetchData = () => {
        APIServices.ReadDataByParam("users", localStorage.getItem("username"))
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    if (
                        data.payload.fullName ===
                        localStorage.getItem("fullName")
                    ) {
                        this.setState({
                            userData: data.payload,
                        });
                        APIServices.ReadData(
                            "invoice?userId=" +
                                data.payload.id +
                                "&size=5&page=" +
                                this.state.page
                        )
                            .then((response) => response.json())
                            .then((d) => {
                                if (d.first === true && d.empty === true) {
                                    this.setState({ isEmpty: true });
                                } else {
                                    this.setState({
                                        ...this.state,
                                        invoice: d.content,
                                        totalPages: d.totalPages,
                                        isEmpty: false,
                                    });
                                }
                            });
                    } else {
                        localStorage.clear();
                        window.location.reload();
                    }
                }
            });
    };

    componentDidMount() {
        this.fetchData();
    }

    handlePagePlus = () => {
        this.setState(
            (prevState) => ({ page: prevState.page + 1 }),
            () => this.fetchData()
        );
    };

    handlePageMin = () => {
        this.setState(
            (prevState) => ({ page: prevState.page - 1 }),
            () => this.fetchData()
        );
    };

    render() {
        return (
            <>
                <ButtonNavbar navbarUse='invoice' />
                <div className='table-container'>
                    <h2>{localStorage.getItem("fullName")}'s Sales Invoice</h2>
                    <table className='sales-invoice'>
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Gross Amount (Rp)</th>
                                <th>Discount (Rp)</th>
                                <th>Tax (Rp)</th>
                                <th>Invoice (Rp)</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.isEmpty === true ? (
                                <tr>
                                    <td colSpan='8'>
                                        No invoice yet. Once you'll create your
                                        first invoice, you'll be able to view it
                                        here.
                                    </td>
                                </tr>
                            ) : (
                                this.state.invoice.map((data, key) => (
                                    <tr key={key}>
                                        <td>{data.date}</td>
                                        <td id='left'>{data.customer}</td>
                                        <td id='right'>
                                            {data.totalItem.toLocaleString(
                                                "id-ID"
                                            )}
                                        </td>
                                        <td id='right'>
                                            {data.discount.toLocaleString(
                                                "id-ID"
                                            )}
                                        </td>
                                        <td id='right'>
                                            {data.tax.toLocaleString("id-ID")}
                                        </td>
                                        <td id='right'>
                                            {data.totalInvoicePrice.toLocaleString(
                                                "id-ID"
                                            )}
                                        </td>
                                        <td>{data.status}</td>
                                        <td>
                                            <a
                                                href={
                                                    "/sales/" +
                                                    this.state.userData
                                                        .username +
                                                    "/detail/" +
                                                    data.id
                                                }
                                            >
                                                <Button
                                                    type='button'
                                                    value='Detail'
                                                    className='button--box primary'
                                                    onClick={() =>
                                                        this.props.handleClick(
                                                            data.id
                                                        )
                                                    }
                                                />
                                            </a>
                                        </td>
                                    </tr>
                                ))
                            )}
                        </tbody>
                    </table>
                    {this.state.totalPages === "" ||
                    this.state.totalPages === 1 ? null : (
                        <div className='center-pagination'>
                            <div className='pagination-sales'>
                                <span
                                    className={
                                        this.state.page === 0
                                            ? "arrow not-allowed"
                                            : "arrow"
                                    }
                                    onClick={this.handlePageMin}
                                >
                                    &laquo;
                                </span>
                                <span>
                                    Page {this.state.page + 1} of{" "}
                                    {this.state.totalPages}
                                </span>
                                <span
                                    className={
                                        this.state.page + 1 ===
                                        this.state.totalPages
                                            ? "arrow not-allowed"
                                            : "arrow"
                                    }
                                    onClick={this.handlePagePlus}
                                >
                                    &raquo;
                                </span>
                            </div>
                        </div>
                    )}
                </div>
            </>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleClick: (id) => {
            dispatch(setInvoiceId(id));
        },
    };
};

export default connect(null, mapDispatchToProps)(SalesInvoice);
