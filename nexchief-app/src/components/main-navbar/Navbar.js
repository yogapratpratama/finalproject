import React, { Component } from "react";
import "./Navbar.css";
import { Link } from "react-router-dom";
import { BiLogOut } from "react-icons/bi";

class Navbar extends Component {
    logoutUtama = () => {
        localStorage.removeItem("username");
        localStorage.removeItem("fullName");
        window.location.reload();
    };
    
    logoutMini = () => {
        localStorage.removeItem("username");
        localStorage.removeItem("fullName");
        window.location.reload();
    };

    render() {
        return this.props.type === "home" ? (
            <nav className='navbar'>
                <div className='navbar-container'>
                    <img
                        src='./images/nexsoft.png'
                        alt='logo'
                        className='image-logo'
                    ></img>
                    <ul className='nav-menu'>
                        <li className='nav-item'>
                            <Link to='/change-pass' className='nav-links'>
                                Change Password
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <button
                                className='logout-utama'
                                onClick={this.logoutUtama}
                            >
                                <BiLogOut style={{ strokeWidth: 0.6 }} /> Log
                                Out
                            </button>
                        </li>
                    </ul>
                </div>
            </nav>
        ) : (
            <div className='navbar-mini'>
                <ul className='nav-menu-mini'>
                    <li className='nav-item-mini'>
                        <Link to='/' className='nav-links-mini'>
                            Home
                        </Link>
                    </li>
                    <li className='nav-item-mini'>
                        <Link to='/product' className='nav-links-mini'>
                            Product
                        </Link>
                    </li>
                    <li className='nav-item-mini'>
                        <Link to='/sales' className='nav-links-mini'>
                            Sales
                        </Link>
                    </li>
                    <li className='nav-item-mini'>
                        <Link to='/report' className='nav-links-mini'>
                            Report
                        </Link>
                    </li>
                    <li className='nav-item-mini'>
                        <Link to='/dashboard' className='nav-links-mini'>
                            Dashboard
                        </Link>
                    </li>
                </ul>
                <button className='logout' onClick={this.logoutMini}>
                    <BiLogOut style={{ strokeWidth: 0.6 }} /> Logout
                </button>
            </div>
        );
    }
}

export default Navbar;
