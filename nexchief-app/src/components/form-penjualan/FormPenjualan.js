import React, { Component } from "react";
import "../sales-invoice/SalesInvoice.css";
import "./FormPenjualan.css";
import { Button } from "../../atomic-components/button/Button";
import NumberFormat from "react-number-format";
import Input from "../../atomic-components/input/Input";

export default class FormPenjualan extends Component {
    render() {
        return (
            <div className='table-container'>
                <form>
                    <table className='sales-invoice-item'>
                        <tbody>
                            <tr>
                                <th>Date</th>
                                <td>
                                    <Input
                                        type='date'
                                        name='date'
                                        className='invoice-input'
                                        value={this.props.date}
                                        onChange={this.props.handleChange}
                                        disabled={this.props.isDisabled}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <th>Distributor *</th>
                                <td>
                                    <Input
                                        type='text'
                                        name='distributor'
                                        className='invoice-input'
                                        value={this.props.distributor}
                                        onChange={this.props.handleChange}
                                        disabled={this.props.isDisabled}
                                    />
                                    <p className='error-message'>
                                        {this.props.error.distributorError}
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th>Customer *</th>
                                <td>
                                    <Input
                                        type='text'
                                        name='customer'
                                        className='invoice-input'
                                        value={this.props.customer}
                                        onChange={this.props.handleChange}
                                        disabled={this.props.isDisabled}
                                    />
                                    <p className='error-message'>
                                        {this.props.error.customerError}
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    <select
                                        name='status'
                                        value={this.props.status}
                                        onChange={this.props.handleChange}
                                        className='invoice-input'
                                        disabled={this.props.isDisabled}
                                    >
                                        <option value='Unpaid'>Unpaid</option>
                                        <option value='Paid'>Paid</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div className='judul-table'>
                        <h2>ITEM (s)</h2>
                        {this.props.isDisabled ||
                        this.props.errorFetch ? null : (
                            <Button
                                type='button'
                                value='Add Item'
                                className='button--box primary button-add'
                                onClick={this.props.addItem}
                            />
                        )}
                    </div>
                    <table className='sales-invoice'>
                        <thead>
                            <tr>
                                <th style={{ width: "8%" }}>
                                    {this.props.isDisabled ? "No" : "#"}
                                </th>
                                <th style={{ width: "15%" }}>Product Code *</th>
                                <th style={{ width: "28%" }}>Product Name</th>
                                <th style={{ width: "13%" }}>Quantity *</th>
                                <th style={{ width: "13%" }}>
                                    Selling Price (Rp)
                                </th>
                                <th style={{ width: "20%" }}>Subtotal (Rp)</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.errorFetch ? (
                                <tr>
                                    <td colSpan='6'>
                                        Something wrong! Error fetching the
                                        data.
                                    </td>
                                </tr>
                            ) : (
                                this.props.invoiceItems.map(
                                    (invoiceItem, id) => (
                                        <tr key={id}>
                                            <td>
                                                {this.props.isDisabled ? (
                                                    id + 1
                                                ) : (
                                                    <Button
                                                        type='button'
                                                        value='-'
                                                        className='button--box danger minus'
                                                        onClick={() =>
                                                            this.props.removeItem(
                                                                id
                                                            )
                                                        }
                                                        disabled={
                                                            this.props
                                                                .invoiceItems
                                                                .length === 1
                                                        }
                                                    />
                                                )}
                                            </td>
                                            <td>
                                                <select
                                                    name='code'
                                                    value={
                                                        invoiceItem.product.code
                                                    }
                                                    className='invoice-input'
                                                    onChange={(event) =>
                                                        this.props.handleSelect(
                                                            event,
                                                            id
                                                        )
                                                    }
                                                    disabled={
                                                        this.props.isDisabled
                                                    }
                                                >
                                                    <option
                                                        value=''
                                                        defaultValue
                                                        hidden
                                                    >
                                                        Select product code
                                                    </option>
                                                    {this.props.activeProduct.map(
                                                        (data, id) => (
                                                            <option
                                                                value={
                                                                    data.code
                                                                }
                                                                key={id}
                                                            >
                                                                {data.code}
                                                            </option>
                                                        )
                                                    )}
                                                </select>
                                                <p className='error-message'>
                                                    {invoiceItem.product
                                                        .code === ""
                                                        ? this.props.error
                                                              .productCodeError
                                                        : null}
                                                </p>
                                            </td>
                                            <td id='left'>
                                                {invoiceItem.product.name}
                                            </td>
                                            <td>
                                                <NumberFormat
                                                    value={invoiceItem.quantity}
                                                    name='quantity'
                                                    onChange={(event) => {
                                                        this.props.handleChangeItem(
                                                            event,
                                                            id
                                                        );
                                                    }}
                                                    placeholder='0'
                                                    displayType={"input"}
                                                    allowNegative={false}
                                                    thousandSeparator={"."}
                                                    decimalSeparator={false}
                                                    className='invoice-input'
                                                    style={{
                                                        textAlign: "center",
                                                    }}
                                                    disabled={
                                                        invoiceItem.product
                                                            .name === ""
                                                            ? true
                                                            : this.props
                                                                  .isDisabled
                                                    }
                                                />
                                                <p className='error-message'>
                                                    {invoiceItem.quantity ===
                                                        "" ||
                                                    invoiceItem.quantity === 0
                                                        ? this.props.error
                                                              .quantityError
                                                        : null}
                                                </p>
                                            </td>
                                            <td
                                                style={{
                                                    textAlign: "right",
                                                }}
                                            >
                                                {invoiceItem.product.sellingPrice.toLocaleString(
                                                    "id-ID"
                                                )}
                                            </td>
                                            <td
                                                style={{
                                                    textAlign: "right",
                                                }}
                                            >
                                                {isNaN(invoiceItem.subTotal)
                                                    ? 0
                                                    : invoiceItem.subTotal.toLocaleString(
                                                          "id-ID"
                                                      )}
                                            </td>
                                        </tr>
                                    )
                                )
                            )}
                        </tbody>
                    </table>
                    <table className='sales-total-item'>
                        <tbody>
                            <tr>
                                <th>Total Item :</th>
                                <td>
                                    <div className='wrapper-total-item'>
                                        <div className='rupiah'>Rp</div>
                                        <div>
                                            {isNaN(this.props.totalItem)
                                                ? 0
                                                : this.props.totalItem.toLocaleString(
                                                      "id-ID"
                                                  )}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Discount :</th>
                                <td>
                                    <div className='wrapper-total-item'>
                                        <div className='rupiah'>Rp</div>
                                        <div>
                                            <NumberFormat
                                                value={this.props.discount}
                                                type={"text"}
                                                name='discount'
                                                isAllowed={(values) => {
                                                    const {
                                                        formattedValue,
                                                        value,
                                                    } = values;
                                                    return (
                                                        formattedValue === "" ||
                                                        value <=
                                                            this.props.totalItem
                                                    );
                                                }}
                                                onChange={
                                                    this.props.handleChange
                                                }
                                                displayType={"input"}
                                                allowNegative={false}
                                                thousandSeparator={"."}
                                                decimalSeparator={false}
                                                placeholder='0'
                                                className='invoice-input'
                                                style={{ textAlign: "right" }}
                                                disabled={this.props.isDisabled}
                                            />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Tax (10%) :</th>
                                <td>
                                    <div className='wrapper-total-item'>
                                        <div className='rupiah'>Rp</div>
                                        <div>
                                            {this.props.tax.toLocaleString(
                                                "id-ID"
                                            )}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Total :</th>
                                <td>
                                    <div className='wrapper-total-item'>
                                        <div className='rupiah'>Rp</div>
                                        <div>
                                            {this.props.totalInvoicePrice.toLocaleString(
                                                "id-ID"
                                            )}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        );
    }
}
