import React from "react";
import "./FormProduct.css";
import NumberFormat from "react-number-format";
import Input from "../../atomic-components/input/Input";

export default class FormProduct extends React.Component {
    render() {
        return (
            <div className='container'>
                <form id='form-product'>
                    <div className='row'>
                        <div className='col-35'>
                            <Input
                                label='Product Code'
                                type='text'
                                name='code'
                                className='form-input uppercase'
                                placeholder='Enter product code'
                                value={this.props.code}
                                onChange={this.props.handleChange}
                                disabled={
                                    this.props.isEditable === true
                                        ? true
                                        : false
                                }
                            />
                            <p className='error-message'>
                                {this.props.error.codeError}
                            </p>
                        </div>
                        <div className='col-63'>
                            <Input
                                label='Product Name *'
                                type='text'
                                name='name'
                                className='form-input uppercase'
                                placeholder='Enter product name'
                                value={this.props.name}
                                onChange={this.props.handleChange}
                                disabled={this.props.isDisabled}
                            />
                            <p className='error-message'>
                                {this.props.error.nameError}
                            </p>
                        </div>
                    </div>
                    <div className='row'>
                        <Input
                            label='Packaging *'
                            type='text'
                            name='packaging'
                            className='form-input uppercase'
                            placeholder='Enter packaging product'
                            value={this.props.packaging}
                            onChange={this.props.handleChange}
                            disabled={this.props.isDisabled}
                        />
                        <p className='error-message'>
                            {this.props.error.packagingError}
                        </p>
                    </div>
                    <div className='row'>
                        <Input
                            label='Product Description'
                            type='text'
                            name='description'
                            className='form-input uppercase'
                            placeholder='Enter product description'
                            value={this.props.description}
                            onChange={this.props.handleChange}
                            disabled={this.props.isDisabled}
                        />
                    </div>
                    <div
                        className='row'
                        style={{ justifyContent: "center", display: "flex" }}
                    >
                        <div className='col-30'>
                            <div className='input-box'>
                                <label>Product Category *</label>
                                <select
                                    name='category'
                                    className='form-input'
                                    value={this.props.category}
                                    onChange={this.props.handleChange}
                                    disabled={this.props.isDisabled}
                                >
                                    <option value='' selected hidden>
                                        -- PLEASE SELECT --
                                    </option>
                                    <option value='TOP ITEM'>TOP ITEM</option>
                                    <option value='STANDARD'>STANDARD</option>
                                </select>
                            </div>
                            <p className='error-message'>
                                {this.props.error.categoryError}
                            </p>
                        </div>
                        <div className='col-30'>
                        <Input
                                label='Market Launch Date'
                                type='date'
                                name='launchDate'
                                className='form-input'
                                value={this.props.launchDate}
                                onChange={this.props.handleChange}
                                disabled={this.props.isDisabled}
                            />
                        </div>
                        <div className='col-30'>
                            <div className='input-box'>
                                <label>Product Status</label>
                                <select
                                    name='status'
                                    className='form-input'
                                    value={this.props.status}
                                    onChange={this.props.handleChange}
                                    disabled={this.props.isDisabled}
                                >
                                    <option value='ACTIVE'>ACTIVE</option>
                                    <option value='INACTIVE'>INACTIVE</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div
                        className='row'
                        style={{
                            justifyContent: "center",
                            display: "flex",
                        }}
                    >
                        <div className='col-35'>
                            <div className='input-box'>
                                <label>
                                    Standard Buying Price
                                </label>
                                <NumberFormat
                                    value={this.props.buyingPrice}
                                    type={"text"}
                                    name='buyingPrice'
                                    onChange={this.props.handleChange}
                                    displayType={"input"}
                                    allowNegative={false}
                                    thousandSeparator={"."}
                                    decimalSeparator={false}
                                    className='form-input'
                                    placeholder='0'
                                    disabled={this.props.isDisabled}
                                />
                            </div>
                        </div>
                        <div className='col-35'>
                            <div className='input-box'>
                                <label>
                                    Standard Selling Price
                                </label>
                                <NumberFormat
                                    value={this.props.sellingPrice}
                                    type={"text"}
                                    name='sellingPrice'
                                    onChange={this.props.handleChange}
                                    displayType={"input"}
                                    allowNegative={false}
                                    thousandSeparator={"."}
                                    decimalSeparator={false}
                                    className='form-input'
                                    placeholder='0'
                                    disabled={this.props.isDisabled}
                                />
                            </div>
                        </div>
                    </div>
                    <div style={{ borderTop: "1px solid #aaa" }}>
                        <table className='table-timestamp'>
                            <tr>
                                <th>Created At</th>
                                <th>:</th>
                                <td>{this.props.createdAt}</td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <th>:</th>
                                <td>{this.props.createdBy}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <th>:</th>
                                <td>{this.props.updatedAt}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <th>:</th>
                                <td>{this.props.updatedBy}</td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>
        );
    }
}
