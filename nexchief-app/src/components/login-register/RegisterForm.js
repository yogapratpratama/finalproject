import React, { Component } from "react";
import { Link } from "react-router-dom";
import Input from "../../atomic-components/input/Input";
import { Button } from "../../atomic-components/button/Button";

class RegisterForm extends Component {
    render() {
        return (
            <div className='form-container'>
                <form className='form' id='register-form' autoComplete='off'>
                    <img
                        src='./images/nexsoft.png'
                        alt='logo'
                        width='90px'
                    ></img>
                    <h1 className='login'>Create your account</h1>
                    <div className='login-card'>
                        <Input
                            label='Username'
                            className='form-input'
                            type='text'
                            name='username'
                            placeholder='Enter your username'
                            value={this.props.dataToSubmit.username}
                            onChange={this.props.handleChange}
                        />
                        <p className='error-message'>
                            {this.props.error.usernameError}
                        </p>
                    </div>
                    <div className='login-card'>
                        <Input
                            label='Full Name'
                            className='form-input'
                            type='text'
                            name='fullName'
                            placeholder='Enter your full name'
                            value={this.props.dataToSubmit.fullName}
                            onChange={this.props.handleChange}
                        />
                        <p className='error-message'>
                            {this.props.error.fullNameError}
                        </p>
                    </div>
                    <div className='login-card'>
                        <Input
                            label='Email'
                            className='form-input'
                            type='text'
                            name='email'
                            placeholder='Example: xxx@mail.com'
                            value={this.props.dataToSubmit.email}
                            onChange={this.props.handleChange}
                        />
                        <p className='error-message'>
                            {this.props.error.emailError}
                        </p>
                    </div>
                    <div className='login-card'>
                        <Input
                            label='Phone Number'
                            className='form-input'
                            type='number'
                            name='phone'
                            placeholder='Example: 08xxxxxxxxxx'
                            value={this.props.dataToSubmit.phone}
                            onChange={this.props.handleChange}
                        />
                        <p className='error-message'>
                            {this.props.error.phoneError}
                        </p>
                    </div>
                    <div className='login-card'>
                        <Input
                            label='Password'
                            className='form-input'
                            type='password'
                            name='password'
                            placeholder='Enter your password'
                            value={this.props.dataToSubmit.password}
                            onChange={this.props.handleChange}
                        />
                        <p className='error-message'>
                            {this.props.error.passwordError}
                        </p>
                    </div>
                    <div className='login-card'>
                        <Input
                            label='Retype Password'
                            className='form-input'
                            type='password'
                            name='retypePassword'
                            placeholder='Retype your password'
                            value={this.props.dataToSubmit.retypePassword}
                            onChange={this.props.handleChange}
                        />
                        <p className='error-message'>
                            {this.props.error.retypePasswordError}
                        </p>
                    </div>
                    <div className='login-card'>
                        <Button
                            type='submit'
                            className='button--box primary'
                            value='Create Account'
                            style={{ width: "100%" }}
                            onClick={this.props.handleSubmit}
                            form='register-form'
                        />
                    </div>
                    <span className='form-input-login'>
                        Already have an account? Log in <Link to='/'>here</Link>
                    </span>
                </form>
            </div>
        );
    }
}

export default RegisterForm;
