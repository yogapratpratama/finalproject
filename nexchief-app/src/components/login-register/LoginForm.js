import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from "../../atomic-components/button/Button";
import Input from "../../atomic-components/input/Input";
import "./LoginRegister.css";

class LoginForm extends Component {
    render() {
        return (
            <div className='form-container'>
                <form className='form'>
                    <img src='./images/nexsoft.png' alt='logo' width='90px'></img>
                    <h1 className='login'>Log In to NexChief</h1>
                    <p className='error-message center'>{this.props.error}</p>
                    <div className='login-card'>
                        <Input
                            label='Username'
                            className='form-input'
                            type='text'
                            name='username'
                            placeholder='Enter your username'
                            value={this.props.username}
                            onChange={this.props.handleChange}
                        />
                    </div>
                    <div className='login-card'>
                        <Input
                            label='Password'
                            className='form-input'
                            type='password'
                            name='password'
                            placeholder='Enter your password'
                            value={this.props.password}
                            onChange={this.props.handleChange}
                        />
                    </div>
                    <div className='login-card'>
                        <Button
                            type='submit'
                            className='button--box primary'
                            value='Log In'
                            style={{ width: "100%" }}
                            onClick={this.props.handleSubmit}
                        />
                    </div>
                    <span className='form-input-login'>
                        Don&apos;t have an account? Register{" "}
                        <Link to='/register'>here</Link>
                    </span>
                </form>
            </div>
        );
    }
}

export default LoginForm;
