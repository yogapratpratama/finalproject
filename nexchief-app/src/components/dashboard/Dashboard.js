import React, { Component } from "react";
import "./Dashboard.css";
import APIServices from "../../services/APIServices";
import Navbar from "../main-navbar/Navbar";

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            errorFetch: true,
            dashboard: {},
        };
    }

    componentDidMount() {
        APIServices.ReadDataByParam("users", localStorage.getItem("username"))
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    if (
                        data.payload.fullName ===
                        localStorage.getItem("fullName")
                    ) {
                        APIServices.ReadDataByParams(
                            "dashboard/status",
                            "active",
                            "sales",
                            data.payload.id
                        )
                            .then((response) => response.json())
                            .then((data) => {
                                if (data.status === false) {
                                    this.setState({ errorFetch: true });
                                } else {
                                    this.setState({
                                        ...this.state,
                                        dashboard: data.payload,
                                        errorFetch: false,
                                    });
                                }
                            });
                    } else {
                        localStorage.clear();
                        window.location.reload();
                    }
                }
            });
    }

    render() {
        return (
            <div className='dashboard-page'>
                <Navbar />
                <div className='dashboard-container'>
                    <div className='main__title'>
                        <img src='./images/hello.svg' alt='dashboard' />
                        <div className='main__greeting'>
                            <h1>Hello {localStorage.getItem("fullName")}</h1>
                            <p>Welcome to your dashboard</p>
                        </div>
                    </div>

                    <div className='dashboard-cards-container'>
                        <div className='dashboard-card'>
                            <div className='image-inner'>
                                <img
                                    src='./images/sales-1.svg'
                                    alt='sales today'
                                />
                            </div>
                            <div className='card_inner'>
                                <p className='card-title'>Sales (Today)</p>
                                <span className='card-report'>
                                    {this.state.dashboard.salesToday}
                                </span>
                            </div>
                        </div>
                        <div className='dashboard-card'>
                            <div className='image-inner'>
                                <img
                                    src='./images/sales-2.svg'
                                    alt='sales today'
                                />
                            </div>
                            <div className='card_inner'>
                                <p className='card-title'>Sales (This Month)</p>
                                <span className='card-report'>
                                    {this.state.dashboard.salesThisMonth}
                                </span>
                            </div>
                        </div>
                        <div className='dashboard-card'>
                            <div className='image-inner'>
                                <img
                                    src='./images/product.svg'
                                    alt='sales today'
                                />
                            </div>
                            <div className='card_inner'>
                                <p className='card-title'>
                                    Total Product (Active)
                                </p>
                                <span className='card-report'>
                                    {this.state.dashboard.activeProduct}
                                </span>
                            </div>
                        </div>
                        <div className='dashboard-card'>
                            <div className='image-inner'>
                                <img
                                    src='./images/invoice.svg'
                                    alt='sales today'
                                />
                            </div>
                            <div className='card_inner'>
                                <p className='card-title'>
                                    Sales Unpaid (This Month)
                                </p>
                                <span className='card-report'>
                                    {this.state.dashboard.salesThisMonthUnpaid}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
