import React, { Component } from "react";
import "./Homepage.css";
import HomepageMenu from "./HomepageMenu";

export default class Homepage extends Component {
    render() {
        return (
            <div className='homepage'>
                <h2 className='hello'>
                    Welcome {localStorage.getItem("fullName")}
                </h2>
                <div className='homepage-container'>
                    <HomepageMenu />
                    <img
                        src='./images/homepage.png'
                        alt='homepage'
                        className='image-homepage'
                    ></img>
                </div>
            </div>
        );
    }
}
