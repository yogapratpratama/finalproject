import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class HomepageMenu extends Component {
    render() {
        return (
            <div className='homepage-cards'>
                <div className='card'>
                    <Link to='/product'>
                        <p>
                            <img src='./images/food.svg' alt='product'></img>
                            <br />
                            Product
                        </p>
                    </Link>
                </div>
                <div className='card'>
                    <Link to='/sales'>
                        <p>
                            <img src='./images/income.svg' alt='sales'></img>
                            <br />
                            Sales
                        </p>
                    </Link>
                </div>
                <div className='card'>
                    <Link to='/report'>
                        <p>
                            <img src='./images/document.svg' alt='report'></img>
                            <br />
                            Report
                        </p>
                    </Link>
                </div>
                <div className='card'>
                    <Link to='/dashboard'>
                        <p>
                            <img src='./images/home.svg' alt='dashboard'></img>
                            <br />
                            Dashboard
                        </p>
                    </Link>
                </div>
            </div>
        );
    }
}
