import React, { Component } from "react";
import "./ButtonNavbar.css";
import { Link } from "react-router-dom";
import "../main-navbar/Navbar.css";
import { Button } from "../../atomic-components/button/Button";

export default class ButtonNavbar extends Component {
    render() {
        return (
            <div className='navbar-bawah'>
                <div>
                    <Link to='/' className='navbar-logo'>
                        <img
                            src='/images/nexsoft.png'
                            alt='logo'
                            className='image-logo'
                        />
                    </Link>
                </div>
                {this.props.navbarUse === "product" ? (
                    <div className='button-container'>
                        {this.props.isEditable === true &&
                        this.props.isDisabled === false ? (
                            <>
                                <Button
                                    type='submit'
                                    value='Cancel'
                                    className='button--box light'
                                    onClick={this.props.cancelEdit}
                                    style={{ marginRight: "10px" }}
                                />
                                <Button
                                    type='submit'
                                    value='Save'
                                    className='button--box primary'
                                    onClick={this.props.submitHandler}
                                />
                            </>
                        ) : (
                            <>
                                <Button
                                    type='submit'
                                    value={
                                        this.props.id === ""
                                            ? "Submit"
                                            : "Add New"
                                    }
                                    className='button--box primary'
                                    onClick={
                                        this.props.id === ""
                                            ? this.props.submitHandler
                                            : this.props.refreshForm
                                    }
                                />
                                {this.props.id === "" ? null : (
                                    <>
                                        <Button
                                            type='submit'
                                            value='Edit'
                                            className='button--box warning'
                                            form='form-product'
                                            onClick={this.props.editHandler}
                                            style={{ marginLeft: "10px" }}
                                        />
                                        <Button
                                            type='submit'
                                            value='Delete'
                                            className='button--box danger'
                                            onClick={this.props.delete}
                                            style={{ marginLeft: "10px" }}
                                        />
                                    </>
                                )}
                            </>
                        )}
                    </div>
                ) : this.props.navbarUse === "invoice" ? (
                    <div className='button-container'>
                        <Link to='sales/new'>
                            <Button
                                type='submit'
                                value='Add New'
                                className='button--box primary'
                            />
                        </Link>
                    </div>
                ) : this.props.navbarUse === "invoiceNew" ? (
                    <div className='button-container'>
                        <Button
                            type='submit'
                            value='Submit'
                            className='button--box primary'
                            onClick={this.props.invoiceSubmit}
                        />
                    </div>
                ) : this.props.navbarUse === "invoiceDetail" ? (
                    this.props.isDisabled ? (
                        this.props.status === "Paid" ? null : (
                            <Button
                                type='submit'
                                value='Edit'
                                className='button--box warning'
                                onClick={this.props.editHandler}
                            />
                        )
                    ) : (
                        <>
                            <div className='button-container'>
                                <Button
                                    type='submit'
                                    value='Cancel'
                                    className='button--box light'
                                    onClick={this.props.cancelEdit}
                                    style={{ marginRight: "10px" }}
                                />
                                <Button
                                    type='submit'
                                    value='Save'
                                    className='button--box primary'
                                    onClick={this.props.invoiceSubmit}
                                />
                            </div>
                        </>
                    )
                ) : null}
            </div>
        );
    }
}
