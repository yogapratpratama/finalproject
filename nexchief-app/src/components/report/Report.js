import React, { Component } from "react";
import "../dashboard/Dashboard.css";
import Navbar from "../main-navbar/Navbar";
import jsPDF from "jspdf";
import "jspdf-autotable";
import APIServices from "../../services/APIServices";
import SalesReport from "./SalesReport";

export default class Report extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataProduct: [],
            userData: {},
            errorFetch: true,
        };
    }

    componentDidMount() {
        APIServices.ReadDataByParam("users", localStorage.getItem("username"))
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    if (
                        data.payload.fullName ===
                        localStorage.getItem("fullName")
                    ) {
                        this.setState({
                            userData: data.payload,
                        });
                    } else {
                        localStorage.clear();
                        window.location.reload();
                    }
                }
            });
        APIServices.ReadData("products")
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    this.setState({ errorFetch: true });
                } else {
                    this.setState({
                        ...this.state,
                        dataProduct: data.payload,
                        errorFetch: false,
                    });
                }
            });
    }

    exportSalesToPdf = () => {
        let doc = new jsPDF("landscape", "pt", "A4");
        let totalPagesExp = "{total_pages_count_string}";
        doc.autoTable({
            theme: "grid",
            html: "#table-sales",
            columnStyles: { 0: { valign: "middle" } },
            didDrawPage: () => {
                // Header
                doc.setFontSize(20);
                doc.setTextColor(40);

                doc.text("Sales Report", 40, 60);

                // Footer
                var str = "Page " + doc.internal.getNumberOfPages();
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === "function") {
                    str = str + " of " + totalPagesExp;
                }
                doc.setFontSize(10);

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize;
                var pageHeight = pageSize.height
                    ? pageSize.height
                    : pageSize.getHeight();
                doc.text(str, 40, pageHeight - 15);
            },
            margin: { top: 68 },
        });
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
        }
        doc.save("sales_report.pdf");
    };

    exportProductToPdf = () => {
        let doc = new jsPDF("landscape", "pt", "A4");
        let totalPagesExp = "{total_pages_count_string}";
        doc.autoTable({
            theme: "grid",
            head: [
                [
                    "No",
                    "Launch Date",
                    "Code",
                    "Product Name",
                    "Packaging",
                    "Description",
                    "Category",
                    "Status",
                    "Buying Price (Rp)",
                    "Selling Price (Rp)",
                ],
            ],
            body: this.state.dataProduct.map((data, key) => [
                key + 1,
                data.launchDate,
                data.code,
                data.name,
                data.packaging,
                data.description,
                data.category,
                data.status,
                data.buyingPrice.toLocaleString("id-ID"),
                data.sellingPrice.toLocaleString("id-ID"),
            ]),
            didDrawPage: () => {
                // Header
                doc.setFontSize(20);
                doc.setTextColor(40);

                doc.text("Product Report", 40, 60);

                // Footer
                var str = "Page " + doc.internal.getNumberOfPages();
                // Total page number plugin only available in jspdf v1.0+
                if (typeof doc.putTotalPages === "function") {
                    str = str + " of " + totalPagesExp;
                }
                doc.setFontSize(10);

                // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                var pageSize = doc.internal.pageSize;
                var pageHeight = pageSize.height
                    ? pageSize.height
                    : pageSize.getHeight();
                doc.text(str, 40, pageHeight - 15);
            },
            margin: { top: 68 },
        });
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === "function") {
            doc.putTotalPages(totalPagesExp);
        }
        doc.save("product_report.pdf");
    };

    render() {
        return (
            <div className='dashboard-page'>
                <Navbar />
                <div className='dashboard-container'>
                    <div
                        className='main__title'
                        style={{ marginLeft: "150px" }}
                    >
                        <img src='./images/document_report.svg' alt='report' />
                        <div className='main__greeting'>
                            <h1>Export Report</h1>
                        </div>
                    </div>

                    <div className='dashboard-cards-container dashboard-report'>
                        <div
                            className='dashboard-card lebar'
                            onClick={this.exportProductToPdf}
                        >
                            <div className='image-inner center'>
                                <img src='./images/pdf.svg' alt='pdf' />
                            </div>
                            <div className='card_inner center'>
                                <p className='card-title report'>
                                    Export Product Report
                                </p>
                            </div>
                        </div>
                        <div
                            className='dashboard-card lebar'
                            onClick={this.exportSalesToPdf}
                        >
                            <div className='image-inner center'>
                                <img src='./images/pdf.svg' alt='pdf' />
                            </div>
                            <div className='card_inner center'>
                                <p className='card-title report'>
                                    Export Sales Report
                                </p>
                            </div>
                        </div>
                    </div>
                    <div style={{ display: "none" }}>
                        <SalesReport />
                    </div>
                </div>
            </div>
        );
    }
}
