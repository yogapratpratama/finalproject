import React, { Component } from "react";
import "../sales-invoice/SalesInvoice.css";
import APIServices from "../../services/APIServices";

export default class SalesReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            invoice: [],
            errorFetch: true,
        };
    }

    componentDidMount() {
        APIServices.ReadDataByParam("users", localStorage.getItem("username"))
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    if (
                        data.payload.fullName ===
                        localStorage.getItem("fullName")
                    ) {
                        APIServices.ReadDataByParam(
                            "invoice/user",
                            data.payload.id
                        )
                            .then((response) => response.json())
                            .then((d) => {
                                if (d.status === false) {
                                    this.setState({ errorFetch: true });
                                } else {
                                    this.setState({
                                        ...this.state,
                                        invoice: d.payload,
                                        errorFetch: false,
                                    });
                                }
                            });
                    } else {
                        localStorage.clear();
                        window.location.reload();
                    }
                }
            });
    }

    render() {
        return (
            <table className='sales-invoice' id='table-sales'>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Distributor</th>
                        <th>Customer</th>
                        <th>Gross Amount (Rp)</th>
                        <th>Discount (Rp)</th>
                        <th>Tax (Rp)</th>
                        <th>Invoice (Rp)</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.errorFetch ? (
                        <tr>
                            <td colSpan='8'>
                                No invoice yet. Once you'll create your first
                                invoice, you'll be able to view it here.
                            </td>
                        </tr>
                    ) : (
                        this.state.invoice.map((data, key) => (
                            <>
                                <tr key={key}>
                                    <td>{data.date}</td>
                                    <td>{data.distributor}</td>
                                    <td id='left'>{data.customer}</td>
                                    <td id='right'>
                                        {data.totalItem.toLocaleString("id-ID")}
                                    </td>
                                    <td id='right'>
                                        {data.discount.toLocaleString("id-ID")}
                                    </td>
                                    <td id='right'>
                                        {data.tax.toLocaleString("id-ID")}
                                    </td>
                                    <td id='right'>
                                        {data.totalInvoicePrice.toLocaleString(
                                            "id-ID"
                                        )}
                                    </td>
                                    <td>{data.status}</td>
                                </tr>
                                <tr>
                                    <td>Item (s)</td>
                                    <td colSpan='7'>
                                        {data.invoiceItems.map(
                                            (invoiceItem, id) => (
                                                <>
                                                    <br />
                                                    <p key={id}>
                                                        {id + 1}.{" "}
                                                        {
                                                            invoiceItem.product
                                                                .code
                                                        }
                                                        ,{" "}
                                                        {
                                                            invoiceItem.product
                                                                .name
                                                        }
                                                        ,{" "}
                                                        {invoiceItem.quantity.toLocaleString(
                                                            "id-ID"
                                                        )}
                                                        @
                                                        {invoiceItem.product.sellingPrice.toLocaleString(
                                                            "id-ID"
                                                        )}{" "}
                                                        ={" "}
                                                        {invoiceItem.subTotal.toLocaleString(
                                                            "id-ID"
                                                        )}
                                                    </p>
                                                    <br />
                                                </>
                                            )
                                        )}
                                    </td>
                                </tr>
                            </>
                        ))
                    )}
                </tbody>
            </table>
        );
    }
}
