import React, { Component } from "react";
import ChangePasswordContainer from "../container/change-password-container/ChangePasswordContainer";
import { Redirect } from "react-router-dom";

export default class ChangePass extends Component {
    render() {
        return localStorage.getItem("username") === null ||
            localStorage.getItem("fullName") === null ? (
            <Redirect to='/' />
        ) : (
            <ChangePasswordContainer />
        );
    }
}
