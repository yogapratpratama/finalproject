import React, { Component } from "react";
import Report from "../components/report/Report";
import { Redirect } from "react-router-dom";

export default class ReportPage extends Component {
    render() {
        return localStorage.getItem("username") === null ||
            localStorage.getItem("fullName") === null ? (
            <Redirect to='/' />
        ) : (
            <Report />
        );
    }
}
