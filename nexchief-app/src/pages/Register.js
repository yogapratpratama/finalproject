import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import RegisterContainer from "../container/register-container/RegisterContainer";

export default class Register extends Component {
    render() {
        return localStorage.getItem("username") === null ||
            localStorage.getItem("fullName") === null ? (
            <RegisterContainer />
        ) : (
            <Redirect to='/' />
        );
    }
}
