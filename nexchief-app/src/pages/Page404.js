import React, { Component } from "react";
import Page404Container from "../container/404-container/Page404Container";

export default class Page404 extends Component {
    render() {
        return <Page404Container />;
    }
}
