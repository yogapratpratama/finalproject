import React, { Component } from "react";
import Product from "../container/product-container/Product";
import Navbar from "../components/main-navbar/Navbar";
import { Redirect } from "react-router-dom";

export default class ProductPage extends Component {
    render() {
        return localStorage.getItem("username") === null ||
            localStorage.getItem("fullName") === null ? (
            <Redirect to='/' />
        ) : (
            <>
                <Navbar />
                <Product />
            </>
        );
    }
}
