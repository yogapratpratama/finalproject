import React, { Component } from "react";
import Navbar from "../components/main-navbar/Navbar";
import SalesInvoice from "../components/sales-invoice/SalesInvoice";
import { Redirect } from "react-router-dom";

export default class Sales extends Component {
    render() {
        return localStorage.getItem("username") === null ||
            localStorage.getItem("fullName") === null ? (
            <Redirect to='/' />
        ) : (
            <>
                <Navbar />
                <SalesInvoice />
            </>
        );
    }
}
