import React, { Component } from "react";
import Navbar from "../components/main-navbar/Navbar";
import InvoiceNew from "../container/invoice-new-container/InvoiceNew";
import { Redirect } from "react-router-dom";

export default class SalesInvoiceNew extends Component {
    render() {
        return localStorage.getItem("username") === null ||
            localStorage.getItem("fullName") === null ? (
            <Redirect to='/' />
        ) : (
            <>
                <Navbar />
                <InvoiceNew />;
            </>
        );
    }
}
