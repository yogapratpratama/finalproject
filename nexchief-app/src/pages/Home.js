import React, { Component } from "react";
import Homepage from "../components/homepage/Homepage";
import Navbar from "../components/main-navbar/Navbar";
import APIServices from "../services/APIServices";

export default class Home extends Component {
    componentDidMount() {
        APIServices.ReadDataByParam("users", localStorage.getItem("username"))
            .then((response) => response.json())
            .then((data) => {
                if (data.status === false) {
                    localStorage.clear();
                    window.location.reload();
                } else {
                    if (
                        data.payload.fullName !==
                        localStorage.getItem("fullName")
                    ) {
                        localStorage.clear();
                        window.location.reload();
                    }
                }
            });
    }

    render() {
        return (
            <>
                <Navbar type='home' />
                <Homepage />
            </>
        );
    }
}
