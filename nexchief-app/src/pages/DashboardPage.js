import React, { Component } from "react";
import Dashboard from "../components/dashboard/Dashboard";
import { Redirect } from "react-router-dom";

export default class DashboardPage extends Component {
    render() {
        return localStorage.getItem("username") === null ||
            localStorage.getItem("fullName") === null ? (
            <Redirect to='/' />
        ) : (
            <Dashboard />
        );
    }
}
