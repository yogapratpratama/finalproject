import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Navbar from "../components/main-navbar/Navbar";
import InvoiceDetail from "../container/invoice-detail-container/InvoiceDetail";

export default class SalesInvoiceItem extends Component {
    render() {
        return localStorage.getItem("username") === null ||
            localStorage.getItem("fullName") === null ? (
            <Redirect to='/' />
        ) : (
            <>
                <Navbar />
                <InvoiceDetail />;
            </>
        );
    }
}
