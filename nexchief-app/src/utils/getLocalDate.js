export default function getLocalDate() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, "0");
    let MM = String(today.getMonth() + 1).padStart(2, "0");
    let yyyy = today.getFullYear();

    today = yyyy + "-" + MM + "-" + dd;
    return today;
}
