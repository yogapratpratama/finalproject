import { createStore } from "redux";
import rootReducer from "./Reducers";

// STORE
const store = createStore(rootReducer);

export default store;
