const initialState = {
    invoiceId: "",
};

// REDUCER
function rootReducer(state = initialState, action) {
    // invoice id
    if (action.type === "SET_INVOICE_ID") {
        return {
            ...state,
            invoiceId: action.value,
        };
    }
    return state;
}

export default rootReducer;
