import React from "react";
import "./Button.css";

export const Button = (props) => {
    return (
        <input
            type={props.type}
            value={props.value}
            className={props.className}
            onClick={props.onClick}
            style={props.style}
            disabled={props.disabled}
        />
    );
};
