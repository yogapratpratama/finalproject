import React from "react";
import InputBox from "./InputBox";
import "./Input.css";

function Input(props) {
    return (
        <InputBox>
            <label>{props.label}</label>
            <input
                type={props.type}
                name={props.name}
                className={props.className}
                placeholder={props.placeholder}
                value={props.value}
                onChange={props.onChange}
                disabled={props.disabled}
            />
        </InputBox>
    );
}

export default Input;
