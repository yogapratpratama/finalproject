import React from "react";

function InputBox(props) {
    return <div className='input-box'>{props.children}</div>;
}

export default InputBox;
