const ACCESS_API_BASE_URL = "http://localhost:8080/api/";

class APIServices {
    // read data by param
    ReadDataByParam(target, param) {
        return fetch(ACCESS_API_BASE_URL + target + "/" + param, {
            method: "GET",
        });
    }

    // read data by 2 param
    ReadDataByParams(target, param1, info, param2) {
        return fetch(
            ACCESS_API_BASE_URL +
                target +
                "/" +
                param1 +
                "/" +
                info +
                "/" +
                param2,
            {
                method: "GET",
            }
        );
    }

    // read data
    ReadData(target) {
        return fetch(ACCESS_API_BASE_URL + target, {
            method: "GET",
        });
    }

    // create data
    createData(target, data) {
        return fetch(ACCESS_API_BASE_URL + target, {
            method: "POST",
            headers: { "Content-type": "application/json" },
            body: JSON.stringify(data),
        });
    }

    // update data
    updateData(target, data) {
        return fetch(ACCESS_API_BASE_URL + target, {
            method: "PUT",
            headers: { "Content-type": "application/json" },
            body: JSON.stringify(data),
        });
    }

    // delete data
    deleteData(target, id) {
        return fetch(ACCESS_API_BASE_URL + target + "/" + id, {
            method: "DELETE",
        });
    }
}

export default new APIServices();
