package com.nexchief.user.controller;

import java.util.List;

import javax.validation.Valid;

import com.nexchief.user.config.Configuration;
import com.nexchief.user.dto.InvoiceRequest;
import com.nexchief.user.dto.ResponseData;
import com.nexchief.user.model.Invoice;
import com.nexchief.user.service.InvoiceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = Configuration.CROSS_ORIGIN)
@RequestMapping("/api/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    // create new
    @PostMapping
    public ResponseEntity<ResponseData<Invoice>> addInvoice(@Valid @RequestBody InvoiceRequest invoiceRequest,
            Errors errors) {
        return invoiceService.saveInvoice(invoiceRequest, errors);
    }

    // update invoice
    @PutMapping
    public ResponseEntity<ResponseData<Invoice>> updateInvoice(@Valid @RequestBody InvoiceRequest invoiceRequest,
            Errors errors) {
        return invoiceService.saveInvoice(invoiceRequest, errors);
    }

    // retrieve invoice by user
    @GetMapping("user/{userId}")
    public ResponseEntity<ResponseData<List<Invoice>>> findInvoiceByUser(@PathVariable("userId") long userId) {
        return invoiceService.getInvoiceByUser(userId);
    }

    // retrieve invoice by id
    @GetMapping("{id}/user/{userId}")
    public ResponseEntity<ResponseData<Invoice>> findInvoiceByIdAndUserId(@PathVariable("id") long id,
            @PathVariable("userId") long userId) {
        return invoiceService.getInvoiceByIdAndUserId(id, userId);
    }

    // retrieve invoice by user pagination
    @GetMapping
    public Iterable<Invoice> findInvoiceByUserPaging(@RequestParam long userId,
            @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
        Pageable pageable = PageRequest.of(page, size);
        return invoiceService.getInvoiceByUserSorting(userId, pageable);
    }
}
