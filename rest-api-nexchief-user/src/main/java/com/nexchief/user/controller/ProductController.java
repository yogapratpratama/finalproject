package com.nexchief.user.controller;

import java.util.List;

import javax.validation.Valid;

import com.nexchief.user.config.Configuration;
import com.nexchief.user.dto.ProductRequest;
import com.nexchief.user.dto.ResponseData;
import com.nexchief.user.model.Product;
import com.nexchief.user.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = Configuration.CROSS_ORIGIN)
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    // create
    @PostMapping
    public ResponseEntity<ResponseData<Product>> addProduct(@Valid @RequestBody ProductRequest productRequest,
            Errors errors) {
        return productService.saveProduct(productRequest, errors);
    }

    // // retrieve all product
    @GetMapping("/all")
    public ResponseEntity<ResponseData<List<Product>>> getAllProduct() {
        return productService.findAllProduct();
    }

    // retrieve product by status
    @GetMapping("/{status}")
    public ResponseEntity<ResponseData<List<Product>>> getProductsByStatus(@PathVariable("status") String status) {
        return productService.getProductByStatus(status);
    }

    // update product
    @PutMapping
    public ResponseEntity<ResponseData<Product>> updateProduct(@Valid @RequestBody ProductRequest productRequest,
            Errors errors) {
        return productService.editProduct(productRequest, errors);
    }

    // delete product
    @DeleteMapping("/{id}")
    public void deleteOneProduct(@PathVariable("id") Long id) {
        productService.deleteProductById(id);
    }

    // retrieve invoice by user pagination
    @GetMapping
    public Iterable<Product> findBySearch(@RequestParam(required = false) String search,
            @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "3") int size) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("updated_at").descending());
        return productService.findBySearchPagination(search, pageable);
    }
}
