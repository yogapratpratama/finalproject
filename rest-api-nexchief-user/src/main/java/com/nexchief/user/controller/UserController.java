package com.nexchief.user.controller;

import java.util.List;

import javax.validation.Valid;

import com.nexchief.user.config.Configuration;
import com.nexchief.user.dto.ResponseData;
import com.nexchief.user.dto.UserRequest;
import com.nexchief.user.dto.UserResponse;
import com.nexchief.user.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = Configuration.CROSS_ORIGIN)
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    // register
    @PostMapping
    public ResponseEntity<ResponseData<UserResponse>> registerUser(@Valid @RequestBody UserRequest userRequest,
            Errors errors) {
        return userService.registerUser(userRequest, errors);
    }

    // retrieve all users
    @GetMapping
    public ResponseEntity<ResponseData<List<UserResponse>>> getAllUsers() {
        return userService.findAllUsers();
    }

    // login user
    @PostMapping("/login")
    public ResponseEntity<ResponseData<UserResponse>> loginUser(@RequestBody UserRequest userRequest) {
        return userService.login(userRequest);
    }

    // change password user
    @PutMapping("/password")
    public ResponseEntity<ResponseData<UserResponse>> changePass(
            @Valid @RequestBody UserRequest userRequest, Errors errors) {
        return userService.changePassword(userRequest, errors);
    }

    // retrieve invoice by id
    @GetMapping("/{username}")
    public ResponseEntity<ResponseData<UserResponse>> findInvoiceById(@PathVariable("username") String username) {
        return userService.findUser(username);
    }
}
