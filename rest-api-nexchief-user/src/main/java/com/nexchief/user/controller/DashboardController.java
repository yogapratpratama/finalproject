package com.nexchief.user.controller;

import com.nexchief.user.config.Configuration;
import com.nexchief.user.dto.DashboardResponse;
import com.nexchief.user.dto.ResponseData;
import com.nexchief.user.service.DashboardService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = Configuration.CROSS_ORIGIN)
@RequestMapping("/api/dashboard")
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    // count product by status
    @GetMapping("/status/{status}/sales/{userId}")
    public ResponseEntity<ResponseData<DashboardResponse>> getProductsByStatus(@PathVariable("status") String status,
            @PathVariable("userId") Long userId) {
        return dashboardService.getDashboard(status, userId);
    }
}
