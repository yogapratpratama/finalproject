package com.nexchief.user.validator.userValidation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.nexchief.user.dto.UserRequest;

public class PasswordEqualConstraintValidator implements ConstraintValidator<PasswordEqualConstraint, Object> {

    @Override
    public boolean isValid(Object data, ConstraintValidatorContext context) {
        UserRequest userRequest = (UserRequest) data;
        return userRequest.getPassword().equals(userRequest.getRetypePassword());
    }

}
