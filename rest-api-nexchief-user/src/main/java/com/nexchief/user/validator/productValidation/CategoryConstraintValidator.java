package com.nexchief.user.validator.productValidation;

import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CategoryConstraintValidator implements ConstraintValidator<CategoryConstraint, String> {

    List<String> allowed;

    @Override
    public void initialize(CategoryConstraint constraintAnnotation) {
        this.allowed = Arrays.asList(constraintAnnotation.allowed());
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (!allowed.contains(value)) {
            String err = "Category must be one of the following: " + allowed;
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(err).addConstraintViolation();
            return false;
        }
        return true;
    }

}
