package com.nexchief.user.exception;

public class HandleNotFoundException extends RuntimeException {
    public HandleNotFoundException(String message) {
        super(message);
    }
}
