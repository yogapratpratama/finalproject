package com.nexchief.user.exception;

import com.nexchief.user.dto.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(HandleNotFoundException.class)
    public ResponseEntity<ResponseData<?>> customHandleNotFound(Exception ex) {
        ResponseData<?> response = new ResponseData<>();
        response.setStatus(false);
        response.getMessages().add(ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseData<?>> handleAllException(Exception ex) {
        ResponseData<?> response = new ResponseData<>();
        response.setStatus(false);
        response.getMessages().add(ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity<ResponseData<?>> handleBadRequest(Exception ex) {
        ResponseData<?> response = new ResponseData<>();
        response.setStatus(false);
        response.getMessages().add(ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

}
