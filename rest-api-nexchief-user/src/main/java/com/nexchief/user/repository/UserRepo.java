package com.nexchief.user.repository;

import com.nexchief.user.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {

    boolean existsUserByUsername(String username);

    boolean existsUserByEmail(String email);

    boolean existsUserByPhone(String phone);

    User findByUsername(String username);
}
