package com.nexchief.user.repository;

import java.util.List;

import com.nexchief.user.model.Invoice;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InvoiceRepo extends JpaRepository<Invoice, Long> {

    List<Invoice> findByUserId(long userId);

    @Query(nativeQuery = true, value = "SELECT * FROM invoice WHERE user_id = :userId")
    Page<Invoice> findByUserIdContaining(@Param("userId") long userId, Pageable pageable);

    @Query(nativeQuery = true, value = "SELECT * FROM invoice WHERE id= :id and user_id = :userId")
    Invoice findInvoiceByIdAndUserId(@Param("id") long id, @Param("userId") long userId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM invoice WHERE date(date) = current_date() and user_id = :userId")
    Long countBySalesToday(@Param("userId") long userId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM invoice WHERE month(date) = month(curdate()) and user_id = :userId")
    Long countBySalesThisMonth(@Param("userId") long userId);

    @Query(nativeQuery = true, value = "SELECT COUNT(*) FROM invoice WHERE month(date) = month(curdate()) and status='unpaid' and user_id = :userId")
    Long countBySalesUnpaidThisMonth(@Param("userId") long userId);

}
