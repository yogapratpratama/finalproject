package com.nexchief.user.repository;

import java.util.List;

import com.nexchief.user.model.Product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProductRepo extends JpaRepository<Product, Long> {

    @Query(nativeQuery = true, value = "SELECT code FROM product")
    List<String> findAllProductCode();

    List<Product> findByStatus(String status);

    Long countByStatus(String status);

    @Query(nativeQuery = true, value = "SELECT * FROM product WHERE name LIKE %:search% OR code LIKE %:search%")
    Page<Product> findBySearchContaining(@Param("search") String search, Pageable pageable);

}
