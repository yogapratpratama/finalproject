package com.nexchief.user.utils;

public class RandomString {

    // function to generate a random string
    public static String getRandomCode() {

        // chose a Character random from this String
        String Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        // create StringBuffer size of alphabet
        StringBuilder finalCode = new StringBuilder(2);
        StringBuilder firstCode = new StringBuilder();
        StringBuilder secondCode = new StringBuilder();

        for (int i = 0; i < 2; i++) {

            // generate a random alphabet between
            // 0 to alphabet variable length
            int index = (int) (Alphabet.length() * Math.random());

            // add Character one by one in end of stringbbuilder
            firstCode.append(Alphabet.charAt(index));
        }

        for (int i = 0; i < 3; i++) {

            // generate a random alphabet between
            // 0 to alphabet variable length
            int index = (int) (Alphabet.length() * Math.random());

            // add Character one by one in end of sb
            secondCode.append(Alphabet.charAt(index));
        }
        // gabung first code dan second code ke dalam satu string
        finalCode.append(firstCode + "-" + secondCode);
        return finalCode.toString();
    }
}
