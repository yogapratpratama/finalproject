package com.nexchief.user.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import com.nexchief.user.dto.InvoiceRequest;
import com.nexchief.user.dto.ResponseData;
import com.nexchief.user.exception.BadRequestException;
import com.nexchief.user.exception.HandleNotFoundException;
import com.nexchief.user.model.Invoice;
import com.nexchief.user.model.InvoiceItem;
import com.nexchief.user.model.User;
import com.nexchief.user.repository.InvoiceRepo;
import com.nexchief.user.repository.ProductRepo;
import com.nexchief.user.utils.ErrorParsingUtility;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

@Service
@Transactional
public class InvoiceService {

    @Autowired
    private InvoiceRepo invoiceRepo;

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private ModelMapper modelMapper;

    // add invoice and edit invoice
    public ResponseEntity<ResponseData<Invoice>> saveInvoice(@Valid InvoiceRequest invoiceRequest, Errors errors) {
        ResponseData<Invoice> response = new ResponseData<>();
        String[] status = new String[] { "Unpaid", "Paid" };
        List<String> statusList = Arrays.asList(status);

        double totalItem = 0;
        for (InvoiceItem invoiceItem : invoiceRequest.getInvoiceItems()) {
            if (invoiceItem.getQuantity() != Math.round(invoiceItem.getQuantity())) {
                throw new BadRequestException("Do not use float number in quantity");
            }
            double subTotal = invoiceItem.getQuantity()
                    * productRepo.getById(invoiceItem.getProduct().getId()).getSellingPrice();
            invoiceItem.setSubTotal(subTotal);
            totalItem += subTotal;
        }
        invoiceRequest.setTotalItem(totalItem);
        double totalAfterDiscount = totalItem - invoiceRequest.getDiscount();
        double tax = totalAfterDiscount * 10 / 100;
        invoiceRequest.setTax(tax);
        invoiceRequest.setTotalInvoicePrice(totalAfterDiscount + tax);

        if (errors.hasErrors()) {
            response.setStatus(false);
            response.setMessages(ErrorParsingUtility.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        if (invoiceRequest.getDiscount() != Math.round(invoiceRequest.getDiscount())) {
            throw new BadRequestException("Do not use float number in discount");
        }
        if (invoiceRequest.getDiscount() > invoiceRequest.getTotalItem()) {
            throw new BadRequestException("Discount cannot be more than the total item");
        }
        if (invoiceRequest.getDate() == null) {
            invoiceRequest.setDate(LocalDate.now());
        }
        if (invoiceRequest.getStatus() == null || invoiceRequest.getStatus().isEmpty()) {
            invoiceRequest.setStatus("Unpaid");
        }
        if (!statusList.contains(invoiceRequest.getStatus())) {
            throw new BadRequestException("Status must be one of the following: " + statusList);
        }

        Invoice invoice = modelMapper.map(invoiceRequest, Invoice.class);
        invoice = invoiceRepo.save(invoice);

        response.setStatus(true);
        response.getMessages().add("Invoice saved!");
        response.setPayload(invoice);
        return ResponseEntity.ok(response);
    }

    // get invoice by user
    public ResponseEntity<ResponseData<List<Invoice>>> getInvoiceByUser(long userId) {
        ResponseData<List<Invoice>> response = new ResponseData<>();
        List<Invoice> invoiceList = invoiceRepo.findByUserId(userId);
        if (invoiceList.isEmpty()) {
            throw new HandleNotFoundException("Invoice with user ID " + userId + " is not found!");
        }

        for (Invoice invoice : invoiceList) {
            User user = invoice.getUser();
            User userResponse = new User(user.getId(), user.getUsername(), user.getFullName(), null, null, null, null);
            invoice.setUser(userResponse);
        }

        response.setStatus(true);
        response.setPayload(invoiceList);
        return ResponseEntity.ok(response);
    }

    // get invoice by user pagination
    public Iterable<Invoice> getInvoiceByUserSorting(long userId, Pageable pageable) {
        List<Invoice> invoiceList = invoiceRepo.findByUserIdContaining(userId, pageable).getContent();

        for (Invoice invoice : invoiceList) {
            User user = invoice.getUser();
            User userResponse = new User(user.getId(), user.getUsername(), user.getFullName(), null, null, null, null);
            invoice.setUser(userResponse);
        }
        return invoiceRepo.findByUserIdContaining(userId, pageable);
    }

    // get invoice by id and user
    public ResponseEntity<ResponseData<Invoice>> getInvoiceByIdAndUserId(long id, long userId) {
        ResponseData<Invoice> response = new ResponseData<>();
        Invoice invoice = invoiceRepo.findInvoiceByIdAndUserId(id, userId);
        if (invoice == null) {
            throw new HandleNotFoundException("Invoice with ID " + id + " and user ID " + userId + " is not found!");
        }
        User user = invoice.getUser();
        User userResponse = new User(user.getId(), user.getUsername(), user.getFullName(), null, null, null, null);
        invoice.setUser(userResponse);
        response.setStatus(true);
        response.setPayload(invoice);
        return ResponseEntity.ok(response);
    }
}
