package com.nexchief.user.service;

import javax.transaction.Transactional;

import com.nexchief.user.dto.DashboardResponse;
import com.nexchief.user.dto.ResponseData;
import com.nexchief.user.repository.InvoiceRepo;
import com.nexchief.user.repository.ProductRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class DashboardService {

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private InvoiceRepo invoiceRepo;

    public ResponseEntity<ResponseData<DashboardResponse>> getDashboard(String status, Long userId) {
        ResponseData<DashboardResponse> response = new ResponseData<>();
        DashboardResponse dashboardResponse = new DashboardResponse();
        dashboardResponse.setActiveProduct(productRepo.countByStatus(status));
        dashboardResponse.setSalesToday(invoiceRepo.countBySalesToday(userId));
        dashboardResponse.setSalesThisMonth(invoiceRepo.countBySalesThisMonth(userId));
        dashboardResponse.setSalesThisMonthUnpaid(invoiceRepo.countBySalesUnpaidThisMonth(userId));

        response.setStatus(true);
        response.setPayload(dashboardResponse);
        return ResponseEntity.ok(response);
    }
}
