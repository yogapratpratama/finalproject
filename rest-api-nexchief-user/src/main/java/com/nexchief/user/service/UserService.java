package com.nexchief.user.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import com.nexchief.user.dto.ResponseData;
import com.nexchief.user.dto.UserRequest;
import com.nexchief.user.dto.UserResponse;
import com.nexchief.user.exception.BadRequestException;
import com.nexchief.user.exception.HandleNotFoundException;
import com.nexchief.user.model.User;
import com.nexchief.user.repository.UserRepo;
import com.nexchief.user.utils.DateTimeUtility;
import com.nexchief.user.utils.ErrorParsingUtility;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    // register user
    public ResponseEntity<ResponseData<UserResponse>> registerUser(@Valid UserRequest userRequest, Errors errors) {
        ResponseData<UserResponse> response = new ResponseData<>();

        if (errors.hasErrors()) {
            response.setStatus(false);
            response.setMessages(ErrorParsingUtility.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        if (userRepo.existsUserByUsername(userRequest.getUsername())) {
            throw new BadRequestException("This username has already been taken");
        }
        if (userRepo.existsUserByEmail(userRequest.getEmail())) {
            throw new BadRequestException("This email has already been taken");
        }
        if (userRepo.existsUserByPhone(userRequest.getPhone())) {
            throw new BadRequestException("This phone number has already been taken");
        }

        if (userRequest.getRegistrationDate() == null || userRequest.getRegistrationDate() == "") {
            userRequest.setRegistrationDate(DateTimeUtility.getLocalDateTime());
            User user = modelMapper.map(userRequest, User.class);
            String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
            user.setPassword(encodedPassword);
            user = userRepo.save(user);

            response.setStatus(true);
            response.getMessages().add("User saved!");
            response.setPayload(modelMapper.map(user, UserResponse.class));
            return ResponseEntity.ok(response);
        } else {
            throw new BadRequestException("Registration date is read-only");
        }

    }

    // get all user
    public ResponseEntity<ResponseData<List<UserResponse>>> findAllUsers() {
        ResponseData<List<UserResponse>> response = new ResponseData<>();

        List<UserResponse> listUser = new ArrayList<>();
        userRepo.findAll().forEach(user -> {
            listUser.add(modelMapper.map(user, UserResponse.class));
        });
        response.setStatus(true);
        response.setPayload(listUser);
        return ResponseEntity.ok(response);

    }

    // get user by username
    public ResponseEntity<ResponseData<UserResponse>> findUser(String username) {
        ResponseData<UserResponse> response = new ResponseData<>();
        User user = userRepo.findByUsername(username);
        if (!userRepo.existsUserByUsername(username)) {
            throw new HandleNotFoundException("Username " + username + " is not found!");
        }
        response.setStatus(true);
        response.setPayload(modelMapper.map(user, UserResponse.class));
        return ResponseEntity.ok(response);
    }

    // login user
    public ResponseEntity<ResponseData<UserResponse>> login(UserRequest userRequest) {
        ResponseData<UserResponse> response = new ResponseData<>();
        User user = userRepo.findByUsername(userRequest.getUsername());

        if (userRepo.existsUserByUsername(userRequest.getUsername())
                && bCryptPasswordEncoder.matches(userRequest.getPassword(), user.getPassword())) {
            UserResponse userResponse = modelMapper.map(user, UserResponse.class);
            response.setStatus(true);
            response.setPayload(userResponse);
            return ResponseEntity.ok(response);
        } else {
            response.setStatus(false);
            response.getMessages().add("Wrong username or password!");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
    }

    // change password
    public ResponseEntity<ResponseData<UserResponse>> changePassword(@Valid UserRequest userRequest, Errors errors) {
        ResponseData<UserResponse> response = new ResponseData<>();
        Optional<User> prevUser = userRepo.findById(userRequest.getId());
        if (!prevUser.isPresent()) {
            throw new HandleNotFoundException("User with ID " + userRequest.getId() + " is not found");
        }

        if (errors.hasErrors()) {
            response.setStatus(false);
            response.setMessages(ErrorParsingUtility.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        userRequest.setRegistrationDate(prevUser.get().getRegistrationDate());
        
        User user = modelMapper.map(userRequest, User.class);
        String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        user = userRepo.save(user);

        response.setStatus(true);
        response.getMessages().add("password change successful!");
        response.setPayload(modelMapper.map(user, UserResponse.class));
        return ResponseEntity.ok(response);

    }

}
