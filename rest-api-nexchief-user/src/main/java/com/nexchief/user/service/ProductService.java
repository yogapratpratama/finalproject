package com.nexchief.user.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.transaction.Transactional;
import javax.validation.Valid;

import com.nexchief.user.dto.ProductRequest;
import com.nexchief.user.dto.ResponseData;
import com.nexchief.user.exception.BadRequestException;
import com.nexchief.user.exception.HandleNotFoundException;
import com.nexchief.user.model.Product;
import com.nexchief.user.repository.ProductRepo;
import com.nexchief.user.repository.UserRepo;
import com.nexchief.user.utils.DateTimeUtility;
import com.nexchief.user.utils.ErrorParsingUtility;
import com.nexchief.user.utils.RandomString;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductRepo productRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepo userRepo;

    // mendapatkan semua product code untuk dicek
    public List<String> findAllProductCode() {
        return productRepo.findAllProductCode();
    }

    // add product
    public ResponseEntity<ResponseData<Product>> saveProduct(@Valid ProductRequest productRequest, Errors errors) {
        ResponseData<Product> response = new ResponseData<>();
        String[] status = new String[] { "ACTIVE", "INACTIVE" };
        List<String> statusList = Arrays.asList(status);

        if (errors.hasErrors()) {
            response.setStatus(false);
            response.setMessages(ErrorParsingUtility.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        // cek product code, jika tidak di isi akan digenerate product code nya otomatis
        if (productRequest.getCode().isEmpty() || productRequest.getCode() == null) {
            // cek jumlah product dan tambah satu untuk pembuatan kodenya
            int CountProduct = findAllProductCode().size();
            String setId = RandomString.getRandomCode() + "-" + (CountProduct + 1);
            productRequest.setCode(setId);
        }
        if (productRequest.getLaunchDate() == null) {
            productRequest.setLaunchDate(LocalDate.now());
        }
        if (productRequest.getStatus() == null || productRequest.getStatus().isEmpty()) {
            productRequest.setStatus("ACTIVE");
        }

        // validation
        if (!statusList.contains(productRequest.getStatus())) {
            throw new BadRequestException("Status must be one of the following: " + statusList);
        }
        if (productRequest.getBuyingPrice() != Math.round(productRequest.getBuyingPrice())
                || productRequest.getSellingPrice() != Math.round(productRequest.getSellingPrice())) {
            throw new BadRequestException("Do not use float number in price");
        }
        if (findAllProductCode().contains(productRequest.getCode())) {
            throw new BadRequestException("Product code already in use");
        }
        if (!Pattern.matches("^[A-Z]{2,3}-[A-Z]{2,3}-[0-9]*$", productRequest.getCode())) {
            throw new BadRequestException("Invalid product code");
        }
        if (!userRepo.existsUserByUsername(productRequest.getCreatedBy())
                || (!userRepo.existsUserByUsername(productRequest.getUpdatedBy()))) {
            throw new HandleNotFoundException("User not found!");
        }

        // created at dan created by hanya bisa diisi oleh sistem.
        productRequest.setCreatedAt(DateTimeUtility.getLocalDateTime());
        productRequest.setUpdatedAt(DateTimeUtility.getLocalDateTime());
        Product product = modelMapper.map(productRequest, Product.class);
        product = productRepo.save(product);

        response.setStatus(true);
        response.getMessages().add("Product saved!");
        response.setPayload(product);
        return ResponseEntity.ok(response);
    }

    // edit product
    public ResponseEntity<ResponseData<Product>> editProduct(@Valid ProductRequest productRequest, Errors errors) {
        ResponseData<Product> response = new ResponseData<>();
        String[] status = new String[] { "ACTIVE", "INACTIVE" };
        List<String> statusList = Arrays.asList(status);
        Optional<Product> prevProduct = productRepo.findById(productRequest.getId());
        if (!prevProduct.isPresent()) {
            throw new HandleNotFoundException("Product with ID " + productRequest.getId() + " is not found");
        }

        if (errors.hasErrors()) {
            response.setStatus(false);
            response.setMessages(ErrorParsingUtility.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        // validation
        if (productRequest.getLaunchDate() == null) {
            productRequest.setLaunchDate(LocalDate.now());
        }
        if (productRequest.getStatus() == null || productRequest.getStatus().isEmpty()) {
            productRequest.setStatus("ACTIVE");
        }
        if (!statusList.contains(productRequest.getStatus())) {
            throw new BadRequestException("Status must be one of the following: " + statusList);
        }
        if (productRequest.getBuyingPrice() != Math.round(productRequest.getBuyingPrice())
                || productRequest.getSellingPrice() != Math.round(productRequest.getSellingPrice())) {
            throw new BadRequestException("Do not use float number in price");
        }
        if (!userRepo.existsUserByUsername(productRequest.getCreatedBy())
                || (!userRepo.existsUserByUsername(productRequest.getUpdatedBy()))) {
            throw new HandleNotFoundException("User not found!");
        }

        productRequest.setCode(prevProduct.get().getCode());
        productRequest.setCreatedAt(prevProduct.get().getCreatedAt());
        productRequest.setUpdatedAt(DateTimeUtility.getLocalDateTime());
        Product product = modelMapper.map(productRequest, Product.class);
        product = productRepo.save(product);

        response.setStatus(true);
        response.getMessages().add("Product changed successfully!");
        response.setPayload(product);
        return ResponseEntity.ok(response);
    }

    // get all product
    public ResponseEntity<ResponseData<List<Product>>> findAllProduct() {
        ResponseData<List<Product>> response = new ResponseData<>();
        response.setStatus(true);
        response.setPayload(productRepo.findAll());
        return ResponseEntity.ok(response);

    }

    // get invoice by user pagination
    public Iterable<Product> findBySearchPagination(String search, Pageable pageable) {
        return productRepo.findBySearchContaining(search, pageable);
    }

    // get product by status active/inactive
    public ResponseEntity<ResponseData<List<Product>>> getProductByStatus(String status) {
        ResponseData<List<Product>> response = new ResponseData<>();
        List<Product> productList = productRepo.findByStatus(status);
        if (productList.isEmpty()) {
            throw new HandleNotFoundException("Product with status " + status + " is not found!");
        }
        response.setStatus(true);
        response.setPayload(productRepo.findByStatus(status));
        return ResponseEntity.ok(response);
    }

    // delete product by id
    public void deleteProductById(Long id) {
        productRepo.deleteById(id);
    }
}
