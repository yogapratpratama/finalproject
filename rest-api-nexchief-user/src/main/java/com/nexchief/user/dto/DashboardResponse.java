package com.nexchief.user.dto;

public class DashboardResponse {

    private long activeProduct;

    private long salesToday;

    private long salesThisMonth;

    private long salesThisMonthUnpaid;

    public DashboardResponse() {
    }

    public DashboardResponse(long activeProduct, long salesToday, long salesThisMonth, long salesThisMonthUnpaid) {
        this.activeProduct = activeProduct;
        this.salesToday = salesToday;
        this.salesThisMonth = salesThisMonth;
        this.salesThisMonthUnpaid = salesThisMonthUnpaid;
    }

    public long getSalesThisMonthUnpaid() {
        return salesThisMonthUnpaid;
    }

    public void setSalesThisMonthUnpaid(long salesThisMonthUnpaid) {
        this.salesThisMonthUnpaid = salesThisMonthUnpaid;
    }

    public long getActiveProduct() {
        return activeProduct;
    }

    public void setActiveProduct(long activeProduct) {
        this.activeProduct = activeProduct;
    }

    public long getSalesToday() {
        return salesToday;
    }

    public void setSalesToday(long salesToday) {
        this.salesToday = salesToday;
    }

    public long getSalesThisMonth() {
        return salesThisMonth;
    }

    public void setSalesThisMonth(long salesThisMonth) {
        this.salesThisMonth = salesThisMonth;
    }

}
