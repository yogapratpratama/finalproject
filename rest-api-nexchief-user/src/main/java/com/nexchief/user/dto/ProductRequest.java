package com.nexchief.user.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

import com.nexchief.user.validator.productValidation.CategoryConstraint;

public class ProductRequest {

    private long id;

    private String code;

    @NotBlank(message = "Product name is required")
    private String name;

    @NotBlank(message = "Packaging is required")
    private String packaging;

    private String description;

    @NotBlank(message = "Product category is required")
    @CategoryConstraint(allowed = { "TOP ITEM", "STANDARD" })
    private String category;

    private LocalDate launchDate;

    private String status;

    @PositiveOrZero(message = "Buying price must be positive")
    private double buyingPrice;

    @PositiveOrZero(message = "Selling price must be positive")
    private double sellingPrice;

    private String createdAt;
    private String createdBy;
    private String updatedAt;
    private String updatedBy;

    public ProductRequest() {
    }

    public ProductRequest(long id, String code, String name, String packaging, String description, String category,
            LocalDate launchDate, String status, double buyingPrice, double sellingPrice, String createdAt,
            String createdBy, String updatedAt, String updatedBy) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.packaging = packaging;
        this.description = description;
        this.category = category;
        this.launchDate = launchDate;
        this.status = status;
        this.buyingPrice = buyingPrice;
        this.sellingPrice = sellingPrice;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDate getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(LocalDate launchDate) {
        this.launchDate = launchDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getBuyingPrice() {
        return buyingPrice;
    }

    public void setBuyingPrice(double buyingPrice) {
        this.buyingPrice = buyingPrice;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

}
