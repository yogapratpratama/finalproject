package com.nexchief.user.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.nexchief.user.validator.userValidation.PasswordEqualConstraint;

@PasswordEqualConstraint(message = "Retype password didn't match with password!")
public class UserRequest {
    private long id;

    @NotBlank(message = "Username is required")
    @Pattern(regexp = "^[\\w](?!.*?\\.{2})[\\w.]{1,18}[\\w]$", message = "Your username is incorrect")
    private String username;

    @NotBlank(message = "Full name is required")
    private String fullName;

    @NotBlank(message = "Email is required")
    @Email(message = "Invalid email address")
    private String email;

    @NotBlank(message = "Phone number is required")
    @Pattern(regexp = "^(0)8[1-9][0-9]{8,10}$", message = "Invalid phone number")
    private String phone;

    @NotBlank(message = "Password is required")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$", message = "Password must contain minimum 8 characters, one or more uppercase letter, one or more lowercase letter and one or more number")
    private String password;

    @NotBlank(message = "Retype password is required")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$", message = "Password must contain minimum 8 characters, one or more uppercase letter, one or more lowercase letter and one or more number")
    private String retypePassword;

    private String registrationDate;

    public UserRequest() {
    }

    public UserRequest(long id, String username, String fullName, String email, String phone, String password,
            String retypePassword, String registrationDate) {
        this.id = id;
        this.username = username;
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.retypePassword = retypePassword;
        this.registrationDate = registrationDate;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }
}
