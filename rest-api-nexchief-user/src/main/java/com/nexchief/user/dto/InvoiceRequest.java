package com.nexchief.user.dto;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.nexchief.user.model.InvoiceItem;
import com.nexchief.user.model.User;

public class InvoiceRequest {

    private long id;

    private LocalDate date;

    @NotBlank(message = "Distributor is required")
    private String distributor;

    @NotBlank(message = "Customer is required")
    private String customer;

    private String status;
    private double totalItem;
    private double discount;
    private double tax;
    private double totalInvoicePrice;

    @NotNull(message = "user is required")
    private User user;

    @NotEmpty(message = "Invoice item is required")
    private List<InvoiceItem> invoiceItems;

    public InvoiceRequest() {
    }

    public InvoiceRequest(long id, LocalDate date, String distributor, String customer, String status, double totalItem,
            double discount, double tax, double totalInvoicePrice, User user, List<InvoiceItem> invoiceItems) {
        this.id = id;
        this.date = date;
        this.distributor = distributor;
        this.customer = customer;
        this.status = status;
        this.totalItem = totalItem;
        this.discount = discount;
        this.tax = tax;
        this.totalInvoicePrice = totalInvoicePrice;
        this.user = user;
        this.invoiceItems = invoiceItems;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDistributor() {
        return distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<InvoiceItem> getInvoiceItems() {
        return invoiceItems;
    }

    public void setInvoiceItems(List<InvoiceItem> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }

    public double getTotalItem() {
        return totalItem;
    }

    public void setTotalItem(double totalItem) {
        this.totalItem = totalItem;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public double getTotalInvoicePrice() {
        return totalInvoicePrice;
    }

    public void setTotalInvoicePrice(double totalInvoicePrice) {
        this.totalInvoicePrice = totalInvoicePrice;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
